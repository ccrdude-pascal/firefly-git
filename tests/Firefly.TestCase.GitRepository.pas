unit Firefly.TestCase.GitRepository;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testutils,
   testregistry,
   LibGit2.Repository;

type

   { TTestCaseGitRepository }

   TTestCaseGitRepository = class(TTestCase)
   private
      procedure PrepareNewRepo(var APath: string);
   protected
      procedure TearDown; override;
   published
      procedure TestInitNewRepository;
      procedure TestAddFile;
      procedure TestCommit;
   end;

implementation

procedure TTestCaseGitRepository.PrepareNewRepo(var APath: string);
var
   sl: TStringList;
begin
   APath := IncludeTrailingPathDelimiter(APath);
   CleanGitDirectory(APath);
   ForceDirectories(APath);
   sl := TStringList.Create;
   try
      sl.Add('# Firefly Git Test');
      sl.Add('This is just a file added to a local fresh Git repository.');
      sl.SaveToFile(APath + 'README.md');
   finally
      sl.Free;
   end;
end;

procedure TTestCaseGitRepository.TearDown;
var
   sPath: string;
begin
   sPath := ExtractFilePath(ParamStr(0)) + 'testcase-test';
   CleanGitDirectory(sPath);
end;

// covers git_repository_init, git_signature_default, git_repository_index, git_index_write_tree, git_tree_lookup, git_commit_create, git_index_free, git_signature_free
procedure TTestCaseGitRepository.TestInitNewRepository;
var
   repo: TGitRepository;
   sPath: string;
begin
   sPath := ExtractFilePath(ParamStr(0)) + 'testcase-test';
   PrepareNewRepo(sPath);
   repo := TGitRepository.Create;
   try
      repo.Lib.Calls.Clear;
      CheckTrue(repo.InitializeInPath(sPath, False), 'InitializeInPath');
   finally
      repo.Free;
   end;
end;

procedure TTestCaseGitRepository.TestAddFile;
var
   repo: TGitRepository;
   sPath: string;
begin
   sPath := ExtractFilePath(ParamStr(0)) + 'testcase-test';
   PrepareNewRepo(sPath);
   repo := TGitRepository.Create;
   try
      repo.Lib.Calls.Clear;
      CheckTrue(repo.InitializeInPath(sPath, False), 'InitializeInPath');
      CheckTrue(repo.AddFile('README.md'), 'AddFile');
      CheckEquals(1, repo.Index.Count, 'Index.Count');
   finally
      repo.Free;
   end;
end;

procedure TTestCaseGitRepository.TestCommit;
var
   repo: TGitRepository;
   sPath: string;
begin
   sPath := ExtractFilePath(ParamStr(0)) + 'testcase-test';
   PrepareNewRepo(sPath);
   repo := TGitRepository.Create;
   try
      repo.Lib.Calls.Clear;
      CheckTrue(repo.InitializeInPath(sPath, False), 'InitializeInPath');
      CheckTrue(repo.AddFile('README.md'), 'AddFile');
      CheckEquals(1, repo.Index.Count, 'Index.Count');
      CheckTrue(repo.Commit('Added README.md'), 'Commit');
      CheckEquals(2, repo.Commits.Count, 'Commits.Count');
   finally
      repo.Free;
   end;
end;


initialization

   RegisterTest(TTestCaseGitRepository);
end.
