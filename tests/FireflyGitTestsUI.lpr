program FireflyGitTestsUI;

{$mode objfpc}{$H+}

uses
   Interfaces,
   Forms,
   Firefly.TestCase.GitRepository,
   GuiTestRunner,
   Firefly.TestCase.GitLibrary,
   LibGit2.Branch,
   LibGit2.Commits,
   LibGit2.Consts,
   LibGit2.Exceptions,
   LibGit2.Index,
   LibGit2.Lib,
   LibGit2.Remote,
   LibGit2.Repository,
   LibGit2.Status;

   {$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TGuiTestRunner, TestRunner);
   Application.Run;
end.
