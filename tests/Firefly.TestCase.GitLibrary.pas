{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Code tests for the Git library imports.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-31  pk    5m  Added header.
// *****************************************************************************
   )
}    unit Firefly.TestCase.GitLibrary;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testutils,
   testregistry,
   LibGit2.Lib;

type

   { TLibGit2LibraryDebugger }

   TLibGit2LibraryDebugger = class(TLibGit2Library)
   public
      property Imports: TLibGit2Imports read FImports;
      property Handle: TLibHandle read FHandle;
   end;

   { TLibGit2ImportsHacker }

   TLibGit2ImportsHacker = class(TLibGit2Imports)
   public
      function AssignLibGit2Imports: boolean;
      function AssignBranchImports: boolean;
      function AssignCommitImports: boolean;
      function AssignIndexImports: boolean;
      function AssignReferenceImports: boolean;
      function AssignRemoteImports: boolean;
      function AssignRepositoryImports: boolean;
      function AssignRevWalkImports: boolean;
      function AssignSignatureImports: boolean;
      function AssignStatusImports: boolean;
      function AssignTreeImports: boolean;
   end;

   { TTestCaseGitLibrary }

   TTestCaseGitLibrary = class(TTestCase)
   protected
      FLibrary: TLibGit2LibraryDebugger;
      procedure SetUp; override;
      procedure TearDown; override;
   published
      procedure TestHandle;
      procedure TestInitShutdownCount;
      procedure TestAssignLibGit2Imports;
      procedure TestAssignBranchImports;
      procedure TestAssignCommitImports;
      procedure TestAssignIndexImports;
      procedure TestAssignReferenceImports;
      procedure TestAssignRemoteImports;
      procedure TestAssignRepositoryImports;
      procedure TestAssignRevWalkImports;
      procedure TestAssignSignatureImports;
      procedure TestAssignStatusImports;
      procedure TestAssignTreeImports;
      procedure TestAssignImports;
   end;

implementation

{ TLibGit2ImportsHacker }

function TLibGit2ImportsHacker.AssignLibGit2Imports: boolean;
begin
  Result := inherited AssignLibGit2Imports;
end;

function TLibGit2ImportsHacker.AssignBranchImports: boolean;
begin
   Result := inherited AssignBranchImports;
end;

function TLibGit2ImportsHacker.AssignCommitImports: boolean;
begin
   Result := inherited AssignCommitImports;
end;

function TLibGit2ImportsHacker.AssignIndexImports: boolean;
begin
   Result := inherited AssignIndexImports;
end;

function TLibGit2ImportsHacker.AssignReferenceImports: boolean;
begin
   Result := inherited AssignReferenceImports;
end;

function TLibGit2ImportsHacker.AssignRemoteImports: boolean;
begin
   Result := inherited AssignRemoteImports;
end;

function TLibGit2ImportsHacker.AssignRepositoryImports: boolean;
begin
   Result := inherited AssignRepositoryImports;
end;

function TLibGit2ImportsHacker.AssignRevWalkImports: boolean;
begin
   Result := inherited AssignRevWalkImports;
end;

function TLibGit2ImportsHacker.AssignSignatureImports: boolean;
begin
   Result := inherited AssignSignatureImports;
end;

function TLibGit2ImportsHacker.AssignStatusImports: boolean;
begin
   Result := inherited AssignStatusImports;
end;

function TLibGit2ImportsHacker.AssignTreeImports: boolean;
begin
   Result := inherited AssignTreeImports;
end;

procedure TTestCaseGitLibrary.SetUp;
begin
   inherited SetUp;
   FLibrary := TLibGit2LibraryDebugger.Create;
end;

procedure TTestCaseGitLibrary.TearDown;
begin
   FLibrary.Free;
   inherited TearDown;
end;

procedure TTestCaseGitLibrary.TestHandle;
begin
   CheckTrue(FLibrary.Handle > 0, 'Handle');
end;

procedure TTestCaseGitLibrary.TestAssignImports;
begin
   CheckTrue(FLibrary.Imports.AssignImports, 'AssignImports');
end;

procedure TTestCaseGitLibrary.TestAssignLibGit2Imports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignLibGit2Imports, 'AssignLibGit2Imports');
end;

procedure TTestCaseGitLibrary.TestAssignBranchImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignBranchImports, 'AssignBranchImports');
end;

procedure TTestCaseGitLibrary.TestAssignCommitImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignCommitImports, 'AssignCommitImports');
end;

procedure TTestCaseGitLibrary.TestAssignIndexImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignIndexImports, 'AssignIndexImports');
end;

procedure TTestCaseGitLibrary.TestAssignReferenceImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignReferenceImports, 'AssignReferenceImports');
end;

procedure TTestCaseGitLibrary.TestAssignRemoteImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignRemoteImports, 'AssignRemoteImports');
end;

procedure TTestCaseGitLibrary.TestAssignRepositoryImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignRepositoryImports, 'AssignRepositoryImports');
end;

procedure TTestCaseGitLibrary.TestAssignRevWalkImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignRevWalkImports, 'AssignRevWalkImports');
end;

procedure TTestCaseGitLibrary.TestAssignSignatureImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignSignatureImports, 'AssignSignatureImports');
end;

procedure TTestCaseGitLibrary.TestAssignStatusImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignStatusImports, 'AssignStatusImports');
end;

procedure TTestCaseGitLibrary.TestAssignTreeImports;
begin
   CheckTrue(TLibGit2ImportsHacker(FLibrary.Imports).AssignTreeImports, 'AssignTreeImports');
end;

procedure TTestCaseGitLibrary.TestInitShutdownCount;
var
   i: integer;
begin
   i := FLibrary.git_libgit2_init();
   CheckEquals(2, i, 'git_libgit2_init');
   i := FLibrary.git_libgit2_shutdown();
   CheckEquals(1, i, 'git_libgit2_shutdown');
end;


initialization

   RegisterTest(TTestCaseGitLibrary);
end.
