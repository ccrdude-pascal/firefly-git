
# Firefly Git SDK

This repository contains FreePascal code to access the library version of git, the version control system.

## Dependencies

* Firefly-OVM: This allows git list items to be displayed in e.g. TListView with a single line of code.