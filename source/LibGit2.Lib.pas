{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(libgit2 library imports.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk   15m  Started creating own imports.
// *****************************************************************************
   )
}

unit LibGit2.Lib;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}
{$MODESWITCH ADVANCEDRECORDS}

interface

uses
   Classes,
   SysUtils,
   LibGit2.Consts,
   LibGit2.Exceptions;

type
   PGitRepository = Pointer;

   TGitSignature = record
      SigName: pansichar;
      SigEmail: pansichar;
      SigTime: TGitTime;
   end;
   PGitSignature = ^TGitSignature;
   PGitIndex = Pointer;
   PGitTree = Pointer;
   PGitCommit = Pointer;
   PPGitCommit = ^PGitCommit;
   PGitStatusList = Pointer;
   PGitRemote = Pointer;

   PGitStatusOptions = Pointer;

   { TGitObjectID }

   TGitObjectID = record
      {$IFDEF GIT_EXPERIMENTAL_SHA256}
      (** type of object id *)
      type_: byte;
      {$ENDIF}
      (** raw binary formatted id  *)
      id: array[0..GIT_OID_MAX_SIZE - 1] of byte;
      function ToHex: string;
   end;

   TGitIndexTime = record
      seconds: int32_t;
      (* nsec should not be stored as time_t compatible  *)
      nanoseconds: uint32_t;
   end;

   TGitIndexEntry = record
      ctime: TGitIndexTime;
      mtime: TGitIndexTime;
      dev: uint32_t;
      ino: uint32_t;
      mode: uint32_t;
      uid: uint32_t;
      gid: uint32_t;
      file_size: uint32_t;
      id: TGitObjectID;
      flags: uint16_t;
      flags_extended: uint16_t;
      path: pansichar;
   end;
   PGitIndexEntry = ^TGitIndexEntry;

   TGitDiffFile = record
      id: TGitObjectID;
      path: pansichar;
      size: git_object_size_t;
      flags: uint32_t;
      mode: uint16_t;
      id_abbrev: uint16_t;
   end;
   PGitDiffFile = ^TGitDiffFile;

   TGitDiffDelta = record
      status: git_delta_t;
      flags: uint32_t;      (**< git_diff_flag_t values  *)
      similarity: uint16_t; (**< for RENAMED and COPIED, value 0-100  *)
      nfiles: uint16_t;     (**< number of files in this delta  *)
      old_file: TGitDiffFile;
      new_file: TGitDiffFile;
   end;

   PGitDiffDelta = ^TGitDiffDelta;

   TGitStatusEntry = record
      status: git_status_t;
      head_to_index: PGitDiffDelta;
      index_to_workdir: PGitDiffDelta;
   end;

   PGitStatusEntry = ^TGitStatusEntry;

   PGitBranchIterator = Pointer;
   PGitBranch = Pointer;
   PGitReference = Pointer;
   PGitRevWalk = Pointer;

   TGitIndexMatchedPathCallback = function(path, matched_pathspec: pansichar; payload: Pointer): integer; cdecl;

   // libgit2
   Tgit_libgit2_version = function(out major, minor, rev: integer): integer; cdecl;
   Tgit_libgit2_init = function(): integer; cdecl;
   Tgit_libgit2_shutdown = function(): integer; cdecl;
   // branch
   Tgit_branch_is_head = function(branch: PGitReference): integer; cdecl;
   Tgit_branch_is_checked_out = function(branch: PGitReference): integer; cdecl;
   Tgit_branch_iterator_free = procedure(iter: PGitBranchIterator); cdecl;
   Tgit_branch_iterator_new = function(out iterator: PGitBranchIterator; repo: PGitRepository; list_flags: git_branch_t): integer; cdecl;
   Tgit_branch_name = function(out branchname: pansichar; reference: PGitReference): integer; cdecl;
   Tgit_branch_next = function(out reference: PGitReference; out out_type: git_branch_t; iterator: PGitBranchIterator): integer; cdecl;
   // commit
   Tgit_commit_create = function(out oid: TGitObjectID; repository: PGitRepository; update_ref: pansichar; author, committer: PGitSignature;
      message_encoding, message: pansichar; tree: PGitTree; parent_count: size_t; parents: PPGitCommit): integer; cdecl;
   Tgit_commit_create_v = function(out oid: TGitObjectID; repository: PGitRepository; update_ref: pansichar; author, committer: PGitSignature;
      message_encoding, message: pansichar; tree: PGitTree; parent_count: size_t): integer; cdecl; varargs;
   Tgit_commit_lookup = function(out commit: PGitCommit; repository: PGitRepository; constref id: TGitObjectID): integer; cdecl;
   Tgit_commit_message = function(commit: PGitCommit): pansichar; cdecl;
   Tgit_commit_committer = function(commit: PGitCommit): PGitSignature; cdecl;
   Tgit_commit_author = function(commit: PGitCommit): PGitSignature; cdecl;
   Tgit_commit_free = procedure(commit: PGitCommit); cdecl;
   // index
   Tgit_index_entrycount = function(gitindex: PGitIndex): size_t; cdecl;
   Tgit_index_free = procedure(gitindex: PGitIndex); cdecl;
   Tgit_index_get_byindex = function(gitindex: PGitIndex; indexNo: size_t): PGitIndexEntry; cdecl;
   Tgit_index_write = function(gitindex: PGitIndex): integer; cdecl;
   Tgit_index_write_tree = function(out oid: TGitObjectID; gitindex: PGitIndex): integer; cdecl;
   Tgit_index_add_all = function(gitindex: PGitIndex; constref pathspec: TGitStringArray; flags: cardinal; callback: TGitIndexMatchedPathCallback; payload: Pointer): integer; cdecl;
   // reference
   Tgit_reference_name_to_id = function(out oid: TGitObjectID; repository: PGitRepository; name_: pansichar): integer; cdecl;
   // remote
   Tgit_remote_free = procedure(remote: PGitRemote); cdecl;
   Tgit_remote_list = function(out namearray: TGitStringArray; repository: PGitRepository): integer; cdecl;
   Tgit_remote_lookup = function(out remote: PGitRemote; repository: PGitRepository; name_: pansichar): integer; cdecl;
   Tgit_remote_name = function(remote: PGitRemote): pansichar; cdecl;
   Tgit_remote_pushurl = function(remote: PGitRemote): pansichar; cdecl;
   Tgit_remote_url = function(remote: PGitRemote): pansichar; cdecl;
   // repository
   Tgit_repository_index = function(out gitindex: PGitIndex; repo: PGitRepository): integer; cdecl;
   Tgit_repository_init = function(out repository: PGitRepository; path: pansichar; is_bare: cardinal): integer; cdecl;
   Tgit_repository_open = function(out repository: PGitRepository; path: pansichar): integer; cdecl;
   Tgit_repository_path = function(repo: PGitRepository): pansichar; cdecl;
   Tgit_repository_workdir = function(repo: PGitRepository): pansichar; cdecl;
   // revwalk
   Tgit_revwalk_new = function(out walk: PGitRevWalk; repository: PGitRepository): integer; cdecl;
   Tgit_revwalk_sorting = function(walk: PGitRevWalk; sort_mode: cardinal): integer; cdecl;
   Tgit_revwalk_push_head = function(walk: PGitRevWalk): integer; cdecl;
   Tgit_revwalk_next = function(out oid: TGitObjectID; walk: PGitRevWalk): integer; cdecl;
   Tgit_revwalk_free = procedure(walk: PGitRevWalk); cdecl;
   // signature
   Tgit_signature_default = function(out signature: PGitSignature; repository: PGitRepository): integer; cdecl;
   Tgit_signature_free = procedure(signature: PGitSignature); cdecl;
   // status
   Tgit_status_byindex = function(statuslist: PGitStatusList; indexNo: size_t): PGitStatusEntry; cdecl;
   Tgit_status_list_free = procedure(statuslist: PGitStatusList); cdecl;
   Tgit_status_list_new = function(out statuslist: PGitStatusList; repository: PGitRepository; options: PGitStatusOptions): integer; cdecl;
   Tgit_status_list_entrycount = function(statuslist: PGitStatusList): size_t; cdecl;
   Tgit_status_file = function(out statusflags: cardinal; repository: PGitRepository; path: pansichar): integer; cdecl;
   // tree
   Tgit_tree_lookup = function(out tree: PGitTree; repository: PGitRepository; constref id: TGitObjectID): integer; cdecl;

   { TLibGit2Imports }

   TLibGit2Imports = class
   protected
      FHandle: TLibHandle;
      // libgit2
      Fgit_libgit2_version: Tgit_libgit2_version;
      Fgit_libgit2_init: Tgit_libgit2_init;
      Fgit_libgit2_shutdown: Tgit_libgit2_shutdown;
      // branch
      Fgit_branch_is_head: Tgit_branch_is_head;
      Fgit_branch_is_checked_out: Tgit_branch_is_checked_out;
      Fgit_branch_iterator_free: Tgit_branch_iterator_free;
      Fgit_branch_iterator_new: Tgit_branch_iterator_new;
      Fgit_branch_name: Tgit_branch_name;
      Fgit_branch_next: Tgit_branch_next;
      // commit
      Fgit_commit_create: Tgit_commit_create;
      Fgit_commit_create_v: Tgit_commit_create_v;
      Fgit_commit_lookup: Tgit_commit_lookup;
      Fgit_commit_message: Tgit_commit_message;
      Fgit_commit_committer: Tgit_commit_committer;
      Fgit_commit_author: Tgit_commit_author;
      Fgit_commit_free: Tgit_commit_free;
      // index
      Fgit_index_entrycount: Tgit_index_entrycount;
      Fgit_index_free: Tgit_index_free;
      Fgit_index_get_byindex: Tgit_index_get_byindex;
      Fgit_index_write: Tgit_index_write;
      Fgit_index_write_tree: Tgit_index_write_tree;
      Fgit_index_add_all: Tgit_index_add_all;
      // reference
      Fgit_reference_name_to_id: Tgit_reference_name_to_id;
      // remote
      Fgit_remote_free: Tgit_remote_free;
      Fgit_remote_list: Tgit_remote_list;
      Fgit_remote_lookup: Tgit_remote_lookup;
      Fgit_remote_name: Tgit_remote_name;
      Fgit_remote_pushurl: Tgit_remote_pushurl;
      Fgit_remote_url: Tgit_remote_url;
      // repository
      Fgit_repository_index: Tgit_repository_index;
      Fgit_repository_init: Tgit_repository_init;
      Fgit_repository_open: Tgit_repository_open;
      Fgit_repository_path: Tgit_repository_path;
      Fgit_repository_workdir: Tgit_repository_workdir;
      // revwalk
      Fgit_revwalk_new: Tgit_revwalk_new;
      Fgit_revwalk_sorting: Tgit_revwalk_sorting;
      Fgit_revwalk_push_head: Tgit_revwalk_push_head;
      Fgit_revwalk_next: Tgit_revwalk_next;
      Fgit_revwalk_free: Tgit_revwalk_free;
      // signature
      Fgit_signature_default: Tgit_signature_default;
      Fgit_signature_free: Tgit_signature_free;
      // status
      Fgit_status_byindex: Tgit_status_byindex;
      Fgit_status_list_free: Tgit_status_list_free;
      Fgit_status_list_new: Tgit_status_list_new;
      Fgit_status_list_entrycount: Tgit_status_list_entrycount;
      Fgit_status_file: Tgit_status_file;
      // tree
      Fgit_tree_lookup: Tgit_tree_lookup;
      function AssignLibGit2Imports: boolean;
      function AssignBranchImports: boolean;
      function AssignCommitImports: boolean;
      function AssignIndexImports: boolean;
      function AssignReferenceImports: boolean;
      function AssignRemoteImports: boolean;
      function AssignRepositoryImports: boolean;
      function AssignRevWalkImports: boolean;
      function AssignSignatureImports: boolean;
      function AssignStatusImports: boolean;
      function AssignTreeImports: boolean;
   public
      constructor Create(AHandle: TLibHandle);
      function AssignImports: boolean;
   end;

   { TLibGit2Library }

   TLibGit2Library = class
   private
      FLastResult: integer;
      FRaiseExceptions: boolean;
      FCallLog: TStringList;
      FErrorLog: TStringList;
      FLastCall: string;
      FLastError: string;
      FFilename: string;
   private
      function git_commit_create_v(out TheObjectID: TGitObjectID; ARepository: PGitRepository; update_ref: pansichar; author, committer: PGitSignature;
         message_encoding, message: pansichar; tree: PGitTree; parent_count: size_t; parents: array of const): boolean;
   protected
      FHandle: TLibHandle;
      FImports: TLibGit2Imports;
   public
      constructor Create(AFilename: string = 'git2');
      destructor Destroy; override;
      procedure LogCall(AText: string; ADetails: array of const; AFunction: string; AnErrorCode: integer; EvaluateError: boolean = True); inline;
      property LastResult: integer read FLastResult;
      property LastError: string read FLastError;
      property Calls: TStringList read FCallLog;
      property Errors: TStringList read FErrorLog;
   public
      // libgit2
      function git_libgit2_version(out TheMajor, TheMinor, TheRevision: integer): boolean;
      function git_libgit2_init(): integer;
      function git_libgit2_shutdown(): integer;
      // branch
      function git_branch_is_head(ABranch: PGitReference): boolean;
      function git_branch_is_checked_out(ABranch: PGitReference): boolean;
      procedure git_branch_iterator_free(AnInterator: PGitBranchIterator);
      function git_branch_iterator_new(out AnInterator: PGitBranchIterator; ARepository: PGitRepository; list_flags: git_branch_t): boolean;
      function git_branch_name(out ABranchName: string; AReference: PGitReference): boolean;
      function git_branch_next(out AReference: PGitReference; out out_type: git_branch_t; AnInterator: PGitBranchIterator): boolean;
      // commit
      function git_commit_create(out TheObjectID: TGitObjectID; ARepository: PGitRepository; update_ref: pansichar; author, committer: PGitSignature;
         message_encoding, message: pansichar; tree: PGitTree; parent_count: size_t; parents: PPGitCommit): boolean;
      function git_commit_lookup(out TheCommit: PGitCommit; ARepository: PGitRepository; constref TheObjectID: TGitObjectID): boolean;
      function git_commit_message(ACommit: PGitCommit; out AMessage: string): boolean;
      function git_commit_committer(ACommit: PGitCommit; out ASignature: TGitSignature): boolean;
      function git_commit_author(ACommit: PGitCommit; out ASignature: TGitSignature): boolean;
      procedure git_commit_free(ACommit: PGitCommit);
      // index
      function git_index_entrycount(TheIndex: PGitIndex): size_t;
      procedure git_index_free(AnIndex: PGitIndex);
      function git_index_get_byindex(TheIndex: PGitIndex; TheIndexNumber: size_t): PGitIndexEntry;
      function git_index_write(TheIndex: PGitIndex): boolean;
      function git_index_write_tree(out TheObjectID: TGitObjectID; AnIndex: PGitIndex): boolean;
      function git_index_add_all(TheIndex: PGitIndex; constref APathSpec: TGitStringArray; TheFlags: cardinal; ACallback: TGitIndexMatchedPathCallback; APayload: Pointer): boolean;
      // reference
      function git_reference_name_to_id(out TheObjectID: TGitObjectID; ARepository: PGitRepository; AName: string): boolean;
      // remote
      procedure git_remote_free(ARemote: PGitRemote);
      function git_remote_list(ANames: TStrings; ARepository: PGitRepository): boolean;
      function git_remote_lookup(out ARemote: PGitRemote; ARepository: PGitRepository; AName: string): boolean;
      function git_remote_name(ARemote: PGitRemote; out AName: string): boolean;
      function git_remote_pushurl(ARemote: PGitRemote; out AnURL: string): boolean;
      function git_remote_url(ARemote: PGitRemote; out AnURL: string): boolean;
      // repository
      function git_repository_open(out ARepository: PGitRepository; ThePath: string): boolean;
      function git_repository_index(out TheIndex: PGitIndex; ARepository: PGitRepository): boolean;
      function git_repository_init(out ARepository: PGitRepository; ThePath: string; IsBare: boolean): boolean;
      function git_repository_path(ARepository: PGitRepository; out APath: string): boolean;
      function git_repository_workdir(ARepository: PGitRepository; out APath: string): boolean;
      // revwalk
      function git_revwalk_new(out AWalker: PGitRevWalk; ARepository: PGitRepository): boolean;
      function git_revwalk_sorting(AWalker: PGitRevWalk; TheSortMode: TGitSortMode): boolean;
      function git_revwalk_push_head(AWalker: PGitRevWalk): boolean;
      function git_revwalk_next(out AnObjectID: TGitObjectID; AWalker: PGitRevWalk): boolean;
      procedure git_revwalk_free(AWalker: PGitRevWalk);
      // signature
      function git_signature_default(out TheSignature: PGitSignature; ARepository: PGitRepository): boolean;
      procedure git_signature_free(ASignature: PGitSignature);
      // status
      function git_status_byindex(AStatusList: PGitStatusList; TheIndex: size_t): PGitStatusEntry;
      procedure git_status_list_free(AStatusList: PGitStatusList);
      function git_status_list_new(out AStatusList: PGitStatusList; ARepository: PGitRepository; TheOptions: PGitStatusOptions): boolean;
      function git_status_list_entrycount(AStatusList: PGitStatusList): size_t;
      function git_status_file(out TheStatusFlags: TGitStati; ARepository: PGitRepository; APath: string): boolean;
      // tree
      function git_tree_lookup(out TheTree: PGitTree; ARepository: PGitRepository; constref TheTreeObjectID: TGitObjectID): boolean;
   end;

implementation

{ TGitObjectID }

function TGitObjectID.ToHex: string;
var
   i: integer;
begin
   Result := '';
   for i := Low(id) to High(id) do begin
      Result += LowerCase(IntToHex(id[i], 2));
   end;
end;

{ TLibGit2Imports }

function TLibGit2Imports.AssignLibGit2Imports: boolean;
begin
   Result := True;
   Fgit_libgit2_version := Tgit_libgit2_version(GetProcAddress(FHandle, 'git_libgit2_version'));
   Fgit_libgit2_init := Tgit_libgit2_init(GetProcAddress(FHandle, 'git_libgit2_init'));
   Fgit_libgit2_shutdown := Tgit_libgit2_shutdown(GetProcAddress(FHandle, 'git_libgit2_shutdown'));
   Result := Assigned(Fgit_libgit2_version)  // .
      and Assigned(Fgit_libgit2_init)        // .
      and Assigned(Fgit_libgit2_shutdown)    // .
   ;
end;

function TLibGit2Imports.AssignBranchImports: boolean;
begin
   Result := True;
   Fgit_branch_is_head := Tgit_branch_is_head(GetProcAddress(FHandle, 'git_branch_is_head'));
   Fgit_branch_is_checked_out := Tgit_branch_is_checked_out(GetProcAddress(FHandle, 'git_branch_is_checked_out'));
   Fgit_branch_iterator_free := Tgit_branch_iterator_free(GetProcAddress(FHandle, 'git_branch_iterator_free'));
   Fgit_branch_iterator_new := Tgit_branch_iterator_new(GetProcAddress(FHandle, 'git_branch_iterator_new'));
   Fgit_branch_name := Tgit_branch_name(GetProcAddress(FHandle, 'git_branch_name'));
   Fgit_branch_next := Tgit_branch_next(GetProcAddress(FHandle, 'git_branch_next'));
   Result := Assigned(Fgit_branch_is_head)     // .
      and Assigned(Fgit_branch_is_checked_out) // .
      and Assigned(Fgit_branch_iterator_free)  // .
      and Assigned(Fgit_branch_iterator_new)   // .
      and Assigned(Fgit_branch_name)           // .
      and Assigned(Fgit_branch_next)           // .
   ;
end;

function TLibGit2Imports.AssignCommitImports: boolean;
begin
   Result := True;
   Fgit_commit_create := Tgit_commit_create(GetProcAddress(FHandle, 'git_commit_create'));
   Fgit_commit_create_v := Tgit_commit_create_v(GetProcAddress(FHandle, 'git_commit_create_v'));
   Fgit_commit_lookup := Tgit_commit_lookup(GetProcAddress(FHandle, 'git_commit_lookup'));
   Fgit_commit_message := Tgit_commit_message(GetProcAddress(FHandle, 'git_commit_message'));
   Fgit_commit_committer := Tgit_commit_committer(GetProcAddress(FHandle, 'git_commit_committer'));
   Fgit_commit_author := Tgit_commit_author(GetProcAddress(FHandle, 'git_commit_author'));
   Fgit_commit_free := Tgit_commit_free(GetProcAddress(FHandle, 'git_commit_free'));
   Result := Assigned(Fgit_commit_create)    // .
      and Assigned(Fgit_commit_create_v)     // .
      and Assigned(Fgit_commit_lookup)       // .
      and Assigned(Fgit_commit_message)      // .
      and Assigned(Fgit_commit_committer)    // .
      and Assigned(Fgit_commit_author)       // .
      and Assigned(Fgit_commit_free)         // .
   ;
end;

function TLibGit2Imports.AssignIndexImports: boolean;
begin
   Result := True;
   Fgit_index_entrycount := Tgit_index_entrycount(GetProcAddress(FHandle, 'git_index_entrycount'));
   Fgit_index_free := Tgit_index_free(GetProcAddress(FHandle, 'git_index_free'));
   Fgit_index_get_byindex := Tgit_index_get_byindex(GetProcAddress(FHandle, 'git_index_get_byindex'));
   Fgit_index_write := Tgit_index_write(GetProcAddress(FHandle, 'git_index_write'));
   Fgit_index_write_tree := Tgit_index_write_tree(GetProcAddress(FHandle, 'git_index_write_tree'));
   Fgit_index_add_all := Tgit_index_add_all(GetProcAddress(FHandle, 'git_index_add_all'));
   Result := Assigned(Fgit_index_entrycount)        // .
      and Assigned(Fgit_index_free)                 // .
      and Assigned(Fgit_index_get_byindex)          // .
      and Assigned(Fgit_index_write)                // .
      and Assigned(Fgit_index_write_tree)           // .
      and Assigned(Fgit_index_add_all)              // .
   ;
end;

function TLibGit2Imports.AssignReferenceImports: boolean;
begin
   Result := True;
   Fgit_reference_name_to_id := Tgit_reference_name_to_id(GetProcAddress(FHandle, 'git_reference_name_to_id'));
   Result := Assigned(Fgit_reference_name_to_id);
end;

function TLibGit2Imports.AssignRemoteImports: boolean;
begin
   Result := True;
   Fgit_remote_free := Tgit_remote_free(GetProcAddress(FHandle, 'git_remote_free'));
   Fgit_remote_list := Tgit_remote_list(GetProcAddress(FHandle, 'git_remote_list'));
   Fgit_remote_lookup := Tgit_remote_lookup(GetProcAddress(FHandle, 'git_remote_lookup'));
   Fgit_remote_name := Tgit_remote_name(GetProcAddress(FHandle, 'git_remote_name'));
   Fgit_remote_pushurl := Tgit_remote_pushurl(GetProcAddress(FHandle, 'git_remote_pushurl'));
   Fgit_remote_url := Tgit_remote_url(GetProcAddress(FHandle, 'git_remote_url'));
   Result := Assigned(Fgit_remote_free)           // .
      and Assigned(Fgit_remote_list)              // .
      and Assigned(Fgit_branch_iterator_free)     // .
      and Assigned(Fgit_remote_name)              // .
      and Assigned(Fgit_remote_pushurl)           // .
      and Assigned(Fgit_remote_url)               // .
   ;
end;

function TLibGit2Imports.AssignRepositoryImports: boolean;
begin
   Result := True;
   Fgit_repository_index := Tgit_repository_index(GetProcAddress(FHandle, 'git_repository_index'));
   Fgit_repository_init := Tgit_repository_init(GetProcAddress(FHandle, 'git_repository_init'));
   Fgit_repository_open := Tgit_repository_open(GetProcAddress(FHandle, 'git_repository_open'));
   Fgit_repository_path := Tgit_repository_path(GetProcAddress(FHandle, 'git_repository_path'));
   Fgit_repository_workdir := Tgit_repository_workdir(GetProcAddress(FHandle, 'git_repository_workdir'));
   Result := Assigned(Fgit_repository_index)    // .
      and Assigned(Fgit_repository_init)        // .
      and Assigned(Fgit_repository_open)        // .
      and Assigned(Fgit_repository_path)        // .
      and Assigned(Fgit_repository_workdir)     // .
   ;
end;

function TLibGit2Imports.AssignRevWalkImports: boolean;
begin
   Result := True;
   Fgit_revwalk_new := Tgit_revwalk_new(GetProcAddress(FHandle, 'git_revwalk_new'));
   Fgit_revwalk_sorting := Tgit_revwalk_sorting(GetProcAddress(FHandle, 'git_revwalk_sorting'));
   Fgit_revwalk_push_head := Tgit_revwalk_push_head(GetProcAddress(FHandle, 'git_revwalk_push_head'));
   Fgit_revwalk_next := Tgit_revwalk_next(GetProcAddress(FHandle, 'git_revwalk_next'));
   Fgit_revwalk_free := Tgit_revwalk_free(GetProcAddress(FHandle, 'git_revwalk_free'));
   Result := Assigned(Fgit_revwalk_new)                  // .
      and Assigned(Fgit_revwalk_sorting)                 // .
      and Assigned(Fgit_revwalk_push_head)               // .
      and Assigned(Fgit_revwalk_next)                    // .
      and Assigned(Fgit_revwalk_free)                    // .
   ;
end;

function TLibGit2Imports.AssignSignatureImports: boolean;
begin
   Result := True;
   Fgit_signature_default := Tgit_signature_default(GetProcAddress(FHandle, 'git_signature_default'));
   Fgit_signature_free := Tgit_signature_free(GetProcAddress(FHandle, 'git_signature_free'));
   Result := Assigned(Fgit_signature_default) // ,
      and Assigned(Fgit_signature_free)       // .
   ;
end;

function TLibGit2Imports.AssignStatusImports: boolean;
begin
   Result := True;
   Fgit_status_byindex := Tgit_status_byindex(GetProcAddress(FHandle, 'git_status_byindex'));
   Fgit_status_list_free := Tgit_status_list_free(GetProcAddress(FHandle, 'git_status_list_free'));
   Fgit_status_list_new := Tgit_status_list_new(GetProcAddress(FHandle, 'git_status_list_new'));
   Fgit_status_list_entrycount := Tgit_status_list_entrycount(GetProcAddress(FHandle, 'git_status_list_entrycount'));
   Fgit_status_file := Tgit_status_file(GetProcAddress(FHandle, 'git_status_file'));
   Result := Assigned(Fgit_status_byindex)          // .
      and Assigned(Fgit_status_list_free)           // .
      and Assigned(Fgit_status_list_new)            // .
      and Assigned(Fgit_status_list_entrycount)     // .
      and Assigned(Fgit_status_file)                // .
   ;
end;

function TLibGit2Imports.AssignTreeImports: boolean;
begin
   Result := True;
   Fgit_tree_lookup := Tgit_tree_lookup(GetProcAddress(FHandle, 'git_tree_lookup'));
   Result := Assigned(Fgit_tree_lookup);
end;

constructor TLibGit2Imports.Create(AHandle: TLibHandle);
begin
   FHandle := AHandle;
end;

function TLibGit2Imports.AssignImports: boolean;
begin
   Result := True;
   Result := AssignLibGit2Imports // .
      and AssignBranchImports     // .
      and AssignCommitImports     // .
      and AssignIndexImports      // .
      and AssignReferenceImports  // .
      and AssignRemoteImports     // .
      and AssignRepositoryImports // .
      and AssignRevWalkImports    // .
      and AssignSignatureImports  // .
      and AssignStatusImports     // .
      and AssignTreeImports       // .
   ;
end;

{ TLibGit2Library }

constructor TLibGit2Library.Create(AFilename: string);
begin
   FFilename := AFilename;
   FHandle := LoadLibrary(FFilename);
   FImports := TLibGit2Imports.Create(FHandle);
   FImports.AssignImports;
   FCallLog := TStringList.Create;
   FErrorLog := TStringList.Create;
   FRaiseExceptions := True;
   Self.git_libgit2_init();
end;

destructor TLibGit2Library.Destroy;
begin
   Self.git_libgit2_shutdown();
   FCallLog.Free;
   FErrorLog.Free;
   FImports.Free;
   inherited Destroy;
end;

{
   Opens a repository.

   @param PGitRepository ARepository  Will receive a repository handle.
   @param string ThePath              Path to the repository to open.
   @see https://libgit2.org/libgit2/#HEAD/group/repository/git_repository_open
}
function TLibGit2Library.git_repository_open(out ARepository: PGitRepository; ThePath: string): boolean;
begin
   if not Assigned(FImports.Fgit_repository_open) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_repository_open');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_repository_open(ARepository, pansichar(ansistring(ThePath)));
   LogCall('git_repository_open(repository = %s, path = %s) = %d', [IntToHex(UintPtr(ARepository)), ThePath, FLastResult], 'git_repository_open', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_repository_index(out TheIndex: PGitIndex; ARepository: PGitRepository): boolean;
begin
   if not Assigned(FImports.Fgit_repository_index) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_repository_index');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_repository_index(TheIndex, ARepository);
   LogCall('git_repository_index(index = %s, repository = %s) = %d', [IntToHex(UIntPtr(TheIndex)), IntToHex(UIntPtr(ARepository)), FLastResult], 'git_repository_index', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_repository_init(out ARepository: PGitRepository; ThePath: string; IsBare: boolean): boolean;
begin
   if not Assigned(FImports.Fgit_repository_init) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_repository_init');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_repository_init(ARepository, pansichar(ansistring(ThePath)), cardinal(IsBare));
   LogCall('git_repository_init(repository = %s, path = %s, is_bare = %s) = %d', [IntToHex(UIntPtr(ARepository)), ThePath, BoolToStr(IsBare), FLastResult], 'git_repository_init', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_repository_path(ARepository: PGitRepository; out APath: string): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_repository_path) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_repository_path');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_repository_path(ARepository);
   if Assigned(p) then begin
      Result := True;
      APath := string(ansistring(p));
      LogCall('git_repository_path(repository = %s) = %s', [IntToHex(UIntPtr(ARepository)), APath], 'git_repository_path', 0);
   end else begin
      Result := False;
      APath := '';
      LogCall('git_repository_path(repository = %s) = nil', [IntToHex(UIntPtr(ARepository))], 'git_repository_path', -666, False);
   end;
end;

function TLibGit2Library.git_repository_workdir(ARepository: PGitRepository; out APath: string): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_repository_workdir) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_repository_workdir');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_repository_workdir(ARepository);
   if Assigned(p) then begin
      Result := True;
      APath := string(ansistring(p));
      LogCall('git_repository_workdir(repository = %s) = %s', [IntToHex(UIntPtr(ARepository)), APath], 'git_repository_workdir', 0);
   end else begin
      Result := False;
      APath := '';
      LogCall('git_repository_workdir(repository = %s) = nil', [IntToHex(UIntPtr(ARepository))], 'git_repository_workdir', -666, False);
   end;
end;

function TLibGit2Library.git_revwalk_new(out AWalker: PGitRevWalk; ARepository: PGitRepository): boolean;
begin
   if not Assigned(FImports.Fgit_revwalk_new) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_revwalk_new');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_revwalk_new(AWalker, ARepository);
   LogCall('git_revwalk_new(walker = %s, repository = %s) = %d', [IntToHex(UIntPtr(AWalker)), IntToHex(UIntPtr(ARepository)), FLastResult], 'git_revwalk_new', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_revwalk_sorting(AWalker: PGitRevWalk; TheSortMode: TGitSortMode): boolean;
var
   s: string;
begin
   if not Assigned(FImports.Fgit_revwalk_sorting) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_revwalk_sorting');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_revwalk_sorting(AWalker, cardinal(TheSortMode));
   Str(TheSortMode, s);
   LogCall('git_revwalk_sorting(walker = %s, sortmode = %s) = %d', [IntToHex(UIntPtr(AWalker)), s, FLastResult], 'git_revwalk_sorting', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_revwalk_push_head(AWalker: PGitRevWalk): boolean;
begin
   if not Assigned(FImports.Fgit_revwalk_push_head) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_revwalk_push_head');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_revwalk_push_head(AWalker);
   LogCall('git_revwalk_push_head(walker = %s)', [IntToHex(UIntPtr(AWalker))], 'git_revwalk_push_head', 0);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_revwalk_next(out AnObjectID: TGitObjectID; AWalker: PGitRevWalk): boolean;
begin
   if not Assigned(FImports.Fgit_revwalk_next) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_revwalk_next');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_revwalk_next(AnObjectID, AWalker);
   LogCall('git_revwalk_next(oid = %s, walker = %s) = %d', [AnObjectID.ToHex, IntToHex(UIntPtr(AWalker)), FLastResult], 'git_revwalk_next', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

procedure TLibGit2Library.git_revwalk_free(AWalker: PGitRevWalk);
begin
   if not Assigned(FImports.Fgit_revwalk_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_revwalk_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_revwalk_free(AWalker);
   LogCall('git_revwalk_free(walker = %s)', [IntToHex(UIntPtr(AWalker))], 'git_revwalk_free', 0);
end;

function TLibGit2Library.git_signature_default(out TheSignature: PGitSignature; ARepository: PGitRepository): boolean;
begin
   if not Assigned(FImports.Fgit_signature_default) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_signature_default');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_signature_default(TheSignature, ARepository);
   LogCall('git_signature_default(signature = %s, repository = %s) = %d', [IntToHex(UIntPtr(TheSignature)), IntToHex(UIntPtr(ARepository)), FLastResult], 'git_signature_default', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

procedure TLibGit2Library.git_signature_free(ASignature: PGitSignature);
begin
   if not Assigned(FImports.Fgit_signature_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_signature_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_signature_free(ASignature);
   LogCall('git_signature_free(signature = %s)', [IntToHex(UIntPtr(ASignature))], 'git_signature_free', 0);
end;

function TLibGit2Library.git_status_byindex(AStatusList: PGitStatusList; TheIndex: size_t): PGitStatusEntry;
begin
   if not Assigned(FImports.Fgit_status_byindex) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_status_byindex');
      end else begin
         Result := nil;
         Exit;
      end;
   end;
   Result := FImports.Fgit_status_byindex(AStatusList, TheIndex);
   if Assigned(Result) then begin
      LogCall('git_status_byindex(statuslist = %s, no = %d) = ...', [IntToHex(UIntPtr(AStatusList)), TheIndex], 'git_status_byindex', 0);
   end else begin
      //LogCall('git_status_byindex(statuslist = %s, no = %d) = nil', [IntToHex(UIntPtr(TheIndex)), TheIndex], 'git_status_byindex', 0);
   end;
end;

procedure TLibGit2Library.git_status_list_free(AStatusList: PGitStatusList);
begin
   if not Assigned(FImports.Fgit_status_list_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_status_list_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_status_list_free(AStatusList);
   LogCall('git_status_list_free(statuslist = %s)', [IntToHex(UIntPtr(AStatusList))], 'git_status_list_free', 0);
end;

function TLibGit2Library.git_status_list_new(out AStatusList: PGitStatusList; ARepository: PGitRepository; TheOptions: PGitStatusOptions): boolean;
begin
   if not Assigned(FImports.Fgit_status_list_new) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_status_list_new');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_status_list_new(AStatusList, ARepository, TheOptions);
   LogCall('git_status_list_new(statuslist = %s, repository = %s) = %d', [IntToHex(UIntPtr(AStatusList)), IntToHex(UIntPtr(ARepository)), FLastResult], 'git_status_list_new', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_status_list_entrycount(AStatusList: PGitStatusList): size_t;
begin
   if not Assigned(FImports.Fgit_status_list_entrycount) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_status_list_entrycount');
      end else begin
         Result := 0;
         Exit;
      end;
   end;
   Result := FImports.Fgit_status_list_entrycount(AStatusList);
   LogCall('git_status_list_entrycount(statuslist = %s) = %d', [IntToHex(UIntPtr(AStatusList)), Result], 'git_status_list_entrycount', 0);
end;

function TLibGit2Library.git_status_file(out TheStatusFlags: TGitStati; ARepository: PGitRepository; APath: string): boolean;
var
   status: git_status_t;
begin
   if not Assigned(FImports.Fgit_status_file) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_status_file');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_status_file(status, ARepository, pansichar(ansistring(APath)));
   TheStatusFlags.FromInteger(status);
   LogCall('git_status_file(statusflags = %s, repository = %s, path = %s) = %d', [TheStatusFlags.ToString, IntToHex(UIntPtr(ARepository)), APath, FLastResult], 'git_status_file', FLastResult, false);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_tree_lookup(out TheTree: PGitTree; ARepository: PGitRepository; constref TheTreeObjectID: TGitObjectID): boolean;
begin
   if not Assigned(FImports.Fgit_tree_lookup) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_tree_lookup');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_tree_lookup(TheTree, ARepository, TheTreeObjectID);
   LogCall('git_tree_lookup(tree = %s, repository = %s, id = %s) = %d', [IntToHex(UintPtr(TheTree)), IntToHex(UintPtr(ARepository)), TheTreeObjectID.ToHex, FLastResult],
      'git_tree_lookup', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

procedure TLibGit2Library.LogCall(AText: string; ADetails: array of const; AFunction: string; AnErrorCode: integer; EvaluateError: boolean);
begin
   FLastCall := Format(AText, ADetails);
   FCallLog.Add(FLastCall);
   if EvaluateError then begin
      if (GIT_SUCCESS <> AnErrorCode) and (GIT_ITERATOR_OVER <> AnErrorCode) then begin
         FLastError := FLastCall;
         FErrorLog.Add(FLastError);
         if FRaiseExceptions then begin
            raise TLibGitCallException.Create(AnErrorCode, FLastError, AFunction, AText);
         end;
      end;
   end;
end;

function TLibGit2Library.git_libgit2_version(out TheMajor, TheMinor, TheRevision: integer): boolean;
begin
   if not Assigned(FImports.Fgit_libgit2_version) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_libgit2_version');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_libgit2_version(TheMajor, TheMinor, TheRevision);
   LogCall('git_libgit2_version(major = %d, minor = %d, rev = %d) = %d', [TheMajor, TheMajor, TheRevision, FLastResult], 'git_libgit2_version', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_libgit2_init(): integer;
begin
   if not Assigned(FImports.Fgit_libgit2_init) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_libgit2_init');
         Result := 0;
      end else begin
         Exit;
      end;
   end;
   Result := FImports.Fgit_libgit2_init();
end;

function TLibGit2Library.git_libgit2_shutdown(): integer;
begin
   if not Assigned(FImports.Fgit_libgit2_shutdown) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_libgit2_shutdown');
      end else begin
         Result := 0;
         Exit;
      end;
   end;
   Result := FImports.Fgit_libgit2_shutdown();
end;

function TLibGit2Library.git_branch_is_head(ABranch: PGitReference): boolean;
var
   iResult: integer;
begin
   if not Assigned(FImports.Fgit_branch_is_head) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_branch_is_head');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_branch_is_head(ABranch);
   iResult := 0;
   case FLastResult of
      0: Result := True;
      1: Result := False;
      else
         iResult := FLastResult;
   end;
   LogCall('git_branch_is_head(branch = %s) = %d', [IntToHex(UIntPtr(ABranch)), FLastResult], 'git_branch_is_head', iResult);
end;

function TLibGit2Library.git_branch_is_checked_out(ABranch: PGitReference): boolean;
var
   iResult: integer;
begin
   if not Assigned(FImports.Fgit_branch_is_checked_out) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_branch_is_checked_out');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_branch_is_checked_out(ABranch);
   iResult := 0;
   case FLastResult of
      0: Result := True;
      1: Result := False;
      else
         iResult := FLastResult;
   end;
   LogCall('git_branch_is_checked_out(branch = %s) = %d', [IntToHex(UIntPtr(ABranch)), FLastResult], 'git_branch_is_checked_out', iResult);
end;

procedure TLibGit2Library.git_branch_iterator_free(AnInterator: PGitBranchIterator);
begin
   if not Assigned(FImports.Fgit_branch_iterator_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_branch_iterator_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_branch_iterator_free(AnInterator);
   LogCall('git_branch_iterator_free(iterator = %s)', [IntToHex(UIntPtr(AnInterator))], 'git_branch_iterator_free', 0);
end;

function TLibGit2Library.git_branch_iterator_new(out AnInterator: PGitBranchIterator; ARepository: PGitRepository; list_flags: git_branch_t): boolean;
begin
   if not Assigned(FImports.Fgit_branch_iterator_new) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_branch_iterator_new');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_branch_iterator_new(AnInterator, ARepository, list_flags);
   LogCall('git_branch_iterator_new(iterator = %s, repository = %s) = %d', [IntToHex(UIntPtr(AnInterator)), IntToHex(UIntPtr(ARepository)), FLastResult], 'git_branch_iterator_new', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_branch_name(out ABranchName: string; AReference: PGitReference): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_branch_name) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_branch_name');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_branch_name(p, AReference);
   ABranchName := '';
   Result := (GIT_SUCCESS = FLastResult);
   if Result then begin
      ABranchName := string(ansistring(p));
   end;
   LogCall('git_branch_name(name = %s, reference = %s) = %d', [ABranchName, IntToHex(UIntPtr(AReference)), FLastResult], 'git_branch_name', FLastResult);
end;

function TLibGit2Library.git_branch_next(out AReference: PGitReference; out out_type: git_branch_t; AnInterator: PGitBranchIterator): boolean;
begin
   if not Assigned(FImports.Fgit_branch_next) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_branch_next');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_branch_next(AReference, out_type, AnInterator);
   LogCall('git_branch_next(reference = %s, type = %s, iterator = %s) = %d', [IntToHex(UIntPtr(AReference)), IntToHex(out_type), IntToHex(UIntPtr(AnInterator)), FLastResult],
      'git_branch_next', FLastResult, False);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_commit_create(out TheObjectID: TGitObjectID; ARepository: PGitRepository; update_ref: pansichar; author, committer: PGitSignature;
   message_encoding, message: pansichar; tree: PGitTree; parent_count: size_t; parents: PPGitCommit): boolean;
begin
   if not Assigned(FImports.Fgit_commit_create) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_create');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_commit_create(TheObjectID, ARepository, update_ref, author, committer, message_encoding, message, tree, parent_count, parents);
   LogCall('git_commit_create(objectid = %s, repository = %s) = %d', [TheObjectID.ToHex, IntToHex(UintPtr(ARepository)), FLastResult], 'git_commit_create', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_commit_create_v(out TheObjectID: TGitObjectID; ARepository: PGitRepository; update_ref: pansichar; author, committer: PGitSignature;
   message_encoding, message: pansichar; tree: PGitTree; parent_count: size_t; parents: array of const): boolean;
begin
   if not Assigned(FImports.Fgit_commit_create_v) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_create_v');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   //FLastResult := FImports.Fgit_commit_create_v(TheObjectID, ARepository, update_ref, author, committer, message_encoding, message, tree, parent_count, parents);
   FLastResult := -666;
   LogCall('git_commit_create_v(objectid = %s, repository = %s) = %d', [TheObjectID.ToHex, IntToHex(UintPtr(ARepository)), FLastResult], 'git_commit_create_v', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_commit_lookup(out TheCommit: PGitCommit; ARepository: PGitRepository; constref TheObjectID: TGitObjectID): boolean;
begin
   if not Assigned(FImports.Fgit_commit_lookup) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_lookup');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_commit_lookup(TheCommit, ARepository, TheObjectID);
   LogCall('git_commit_lookup(commit = %s, repository = %s, objectid = %s) = %d', [IntToHex(UIntPtr(TheCommit)), IntToHex(UIntPtr(ARepository)), TheObjectID.ToHex, FLastResult],
      'git_commit_lookup', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_commit_message(ACommit: PGitCommit; out AMessage: string): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_commit_message) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_message');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_commit_message(ACommit);
   if Assigned(p) then begin
      Result := True;
      AMessage := string(ansistring(p));
      LogCall('git_commit_message(commit = %s) = %s', [IntToHex(UIntPtr(ACommit)), AMessage], 'git_commit_message', 0);
   end else begin
      Result := False;
      AMessage := '';
      LogCall('git_commit_message(commit = %s) = nil', [IntToHex(UIntPtr(ACommit))], 'git_commit_message', -666, False);
   end;
end;

function TLibGit2Library.git_commit_committer(ACommit: PGitCommit; out ASignature: TGitSignature): boolean;
var
   p: PGitSignature;
begin
   if not Assigned(FImports.Fgit_commit_committer) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_committer');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_commit_committer(ACommit);
   if Assigned(p) then begin
      Result := True;
      ASignature := p^;
      LogCall('git_commit_committer(commit = %s) = %s', [IntToHex(UIntPtr(ACommit)), ASignature.SigEmail], 'git_commit_committer', 0);
   end else begin
      Result := False;
      LogCall('git_commit_committer(commit = %s) = nil', [IntToHex(UIntPtr(ACommit))], 'git_commit_committer', -666, False);
   end;
end;

function TLibGit2Library.git_commit_author(ACommit: PGitCommit; out ASignature: TGitSignature): boolean;
var
   p: PGitSignature;
begin
   if not Assigned(FImports.Fgit_commit_author) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_author');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_commit_author(ACommit);
   if Assigned(p) then begin
      Result := True;
      ASignature := p^;
      LogCall('git_commit_author(commit = %s) = %s', [IntToHex(UIntPtr(ACommit)), ASignature.SigEmail], 'git_commit_author', 0);
   end else begin
      Result := False;
      LogCall('git_commit_author(commit = %s) = nil', [IntToHex(UIntPtr(ACommit))], 'git_commit_author', -666, False);
   end;
end;

procedure TLibGit2Library.git_commit_free(ACommit: PGitCommit);
begin
   if not Assigned(FImports.Fgit_commit_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_commit_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_commit_free(ACommit);
   LogCall('git_commit_free(commit = %s)', [IntToHex(UIntPtr(ACommit))], 'git_commit_free', 0);
end;

function TLibGit2Library.git_index_entrycount(TheIndex: PGitIndex): size_t;
begin
   if not Assigned(FImports.Fgit_index_entrycount) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_index_entrycount');
      end else begin
         Result := 0;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_index_entrycount(TheIndex);
   LogCall('git_index_entrycount(index = %s) = %d', [IntToHex(UIntPtr(TheIndex)), FLastResult], 'git_index_entrycount', 0);
end;

procedure TLibGit2Library.git_index_free(AnIndex: PGitIndex);
begin
   if not Assigned(FImports.Fgit_index_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_index_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_index_free(AnIndex);
   LogCall('git_index_free(index = %s)', [IntToHex(UIntPtr(AnIndex))], 'git_index_free', 0);
end;

function TLibGit2Library.git_index_get_byindex(TheIndex: PGitIndex; TheIndexNumber: size_t): PGitIndexEntry;
begin
   if not Assigned(FImports.Fgit_index_get_byindex) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_index_get_byindex');
      end else begin
         Result := nil;
         Exit;
      end;
   end;
   Result := FImports.Fgit_index_get_byindex(TheIndex, TheIndexNumber);
   if Assigned(Result) then begin
      LogCall('git_index_get_byindex(index = %s, no = %d) = %s', [IntToHex(UIntPtr(TheIndex)), TheIndexNumber, Result^.path], 'git_index_get_byindex', 0);
   end else begin
      //LogCall('git_index_get_byindex(index = %s, no = %d) = nil', [IntToHex(UIntPtr(TheIndex)), TheIndexNumber], 'git_index_get_byindex', 0);
   end;
end;

function TLibGit2Library.git_index_write(TheIndex: PGitIndex): boolean;
begin
   if not Assigned(FImports.Fgit_index_write) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_index_write');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_index_write(TheIndex);
   LogCall('git_index_write(index = %s) = %d', [IntToHex(UIntPtr(TheIndex)), FLastResult], 'git_index_write', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_index_write_tree(out TheObjectID: TGitObjectID; AnIndex: PGitIndex): boolean;
begin
   if not Assigned(FImports.Fgit_index_write_tree) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_index_write_tree');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_index_write_tree(TheObjectID, AnIndex);
   LogCall('git_index_write_tree(objectid = %s, index = %s) = %d', [TheObjectID.ToHex, IntToHex(UIntPtr(AnIndex)), FLastResult], 'git_index_write_tree', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_index_add_all(TheIndex: PGitIndex; constref APathSpec: TGitStringArray; TheFlags: cardinal; ACallback: TGitIndexMatchedPathCallback; APayload: Pointer): boolean;
begin
   if not Assigned(FImports.Fgit_index_add_all) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_index_add_all');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_index_add_all(TheIndex, APathSpec, TheFlags, ACallback, APayload);
   LogCall('git_index_add_all(index = %s, flags = %d, payload = %s) = %d', [IntToHex(UIntPtr(TheIndex)), TheFlags, IntToHex(UIntPtr(APayload)), FLastResult], 'git_index_add_all', 0);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_reference_name_to_id(out TheObjectID: TGitObjectID; ARepository: PGitRepository; AName: string): boolean;
begin
   if not Assigned(FImports.Fgit_reference_name_to_id) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_reference_name_to_id');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_reference_name_to_id(TheObjectID, ARepository, pansichar(ansistring(AName)));
   LogCall('git_reference_name_to_id(objectid = %s, repository = %s, name = %s) = %d', [TheObjectID.ToHex, IntToHex(UIntPtr(ARepository)), AName, FLastResult],
      'git_reference_name_to_id', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

procedure TLibGit2Library.git_remote_free(ARemote: PGitRemote);
begin
   if not Assigned(FImports.Fgit_remote_free) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_remote_free');
      end else begin
         Exit;
      end;
   end;
   FImports.Fgit_remote_free(ARemote);
   LogCall('git_remote_free(remote = %s)', [IntToHex(UIntPtr(ARemote))], 'git_remote_free', 0);
end;

function TLibGit2Library.git_remote_list(ANames: TStrings; ARepository: PGitRepository): boolean;
var
   remoteList: TGitStringArray;
   pp: PPansiChar;
   p: pansichar;
   i: integer;
begin
   if not Assigned(FImports.Fgit_remote_list) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_remote_list');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   Initialize(remoteList);
   remoteList.Count := 0;
   FLastResult := FImports.Fgit_remote_list(remoteList, ARepository);
   LogCall('git_remote_list(repository = %s) = %d', [IntToHex(UIntPtr(ARepository)), FLastResult], 'git_remote_list', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
   if Result then begin
      pp := remoteList.strings;
      p := pp^;
      for i := 0 to Pred(remoteList.Count) do begin
         ANames.Add(string(ansistring(p)));
      end;
   end;
   // TODO : git_strarray_free(remoteList);
end;

function TLibGit2Library.git_remote_lookup(out ARemote: PGitRemote; ARepository: PGitRepository; AName: string): boolean;
begin
   if not Assigned(FImports.Fgit_remote_lookup) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_remote_lookup');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   FLastResult := FImports.Fgit_remote_lookup(ARemote, ARepository, pansichar(ansistring(AName)));
   LogCall('git_remote_lookup(remote = %s, repository = %s, name = %s) = %d', [IntToHex(UIntPtr(ARemote)), IntToHex(UIntPtr(ARepository)), AName, FLastResult],
      'git_remote_lookup', FLastResult);
   Result := (GIT_SUCCESS = FLastResult);
end;

function TLibGit2Library.git_remote_name(ARemote: PGitRemote; out AName: string): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_remote_name) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_remote_name');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_remote_name(ARemote);
   if Assigned(p) then begin
      Result := True;
      AName := string(ansistring(p));
      LogCall('git_remote_name(remoet = %s) = %s', [IntToHex(UIntPtr(ARemote)), AName], 'git_remote_name', 0);
   end else begin
      Result := False;
      AName := '';
      LogCall('git_remote_name(remoet = %s) = nil', [IntToHex(UIntPtr(ARemote))], 'git_remote_name', -666);
   end;
end;

function TLibGit2Library.git_remote_pushurl(ARemote: PGitRemote; out AnURL: string): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_remote_pushurl) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_remote_pushurl');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_remote_pushurl(ARemote);
   if Assigned(p) then begin
      Result := True;
      AnURL := string(ansistring(p));
      LogCall('git_remote_pushurl(remote = %s) = %s', [IntToHex(UIntPtr(ARemote)), AnURL], 'git_remote_pushurl', 0);
   end else begin
      Result := False;
      AnURL := '';
      LogCall('git_remote_pushurl(remote = %s) = nil', [IntToHex(UIntPtr(ARemote))], 'git_remote_pushurl', -666, False);
   end;
end;

function TLibGit2Library.git_remote_url(ARemote: PGitRemote; out AnURL: string): boolean;
var
   p: pansichar;
begin
   if not Assigned(FImports.Fgit_remote_url) then begin
      if FRaiseExceptions then begin
         raise TLibGitLibraryMissingImportException.Create(Self.FFilename, 'git_remote_url');
      end else begin
         Result := False;
         Exit;
      end;
   end;
   p := FImports.Fgit_remote_url(ARemote);
   if Assigned(p) then begin
      Result := True;
      AnURL := string(ansistring(p));
      LogCall('git_remote_url(remote = %s) = %s', [IntToHex(UIntPtr(ARemote)), AnURL], 'git_remote_url', 0);
   end else begin
      Result := False;
      AnURL := '';
      LogCall('git_remote_url(remote = %s) = nil', [IntToHex(UIntPtr(ARemote))], 'git_remote_url', -666, False);
   end;
end;

end.
