{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git status list access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-31  pk    5m  Added header.
// *****************************************************************************
   )
}
unit LibGit2.Status;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Defaults,
   Generics.Collections,
   {$IFDEF SupportFireflyOVM}
   OVM.ListView.Attributes,
   {$ENDIF SupportFireflyOVM}
   LibGit2.Lib,
   LibGit2.Consts;

type
   TGitstatusList = class;

   { TGitStatus }

   TGitStatus = class
   private
      FFilename: string;
      FList: TGitStatusList;
      FIndexIndex: integer;
      FSimilarity: integer;
      FStati: TGitStati;
   public
      constructor Create(AList: TGitStatusList; AnIndexIndex: integer);
      procedure Load;
      property IndexIndex: integer read FIndexIndex;
   published
      property Filename: string read FFilename;
      property Stati: TGitStati read FStati;
      property Similarity: integer read FSimilarity;
   end;

   { TGitStatusList }

   TGitStatusList = class(TObjectList<TGitStatus>)
   private
      FRepository: PGitRepository;
      FLibrary: TLibGit2Library;
   protected
      FStatusList: PGitStatusList;
   public
      constructor Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
      function Refresh: boolean;
      procedure SortByPath;
   end;

implementation

type

   { TGitStatusPathComparer }

   TGitStatusPathComparer = class(TInterfacedObject, IComparer<TGitStatus>)
      function Compare(const Index1, Index2: TGitStatus): integer;
   end;

   { TGitStatusPathComparer }

function TGitStatusPathComparer.Compare(const Index1, Index2: TGitStatus): integer;
begin
   Result := CompareText(Index1.Filename, Index2.Filename);
end;

{ TGitStatusList }

constructor TGitStatusList.Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
begin
   FRepository := ARepository;
   FLibrary := ALibrary;
end;

function TGitStatusList.Refresh: boolean;
var
   i: integer;
   iCount: integer;
   entry: TGitStatus;
   iResult: integer;
begin
   Result := FLibrary.git_status_list_new(FStatusList, FRepository, nil);
   try
      iCount := FLibrary.git_status_list_entrycount(FStatusList);
      for i := 0 to Pred(iCount) do begin
         entry := TGitStatus.Create(Self, i);
         entry.Load;
         Self.Add(entry);
      end;
   finally
      FLibrary.git_status_list_free(FStatusList);
      FStatusList := nil;
   end;
   Self.SortByPath;
end;

procedure TGitStatusList.SortByPath;
begin
   Self.Sort(TGitStatusPathComparer.Create);
end;

{ TGitStatus }
constructor TGitStatus.Create(AList: TGitStatusList; AnIndexIndex: integer);
begin
   FList := AList;
   FIndexIndex := AnIndexIndex;
end;

procedure TGitStatus.Load;
var
   se: PGitStatusEntry;
   s: string;
   gs: TGitStati;
   entry: TGitStatus;
begin
   se := FList.FLibrary.git_status_byindex(FList.FStatusList, FIndexIndex);
   if Assigned(se^.head_to_index) then begin
      FFilename := se^.head_to_index^.new_file.path;
      FSimilarity := se^.head_to_index^.similarity;
   end else if Assigned(se^.index_to_workdir) then begin
      FFilename := se^.index_to_workdir^.new_file.path;
      FSimilarity := se^.index_to_workdir^.similarity;
   end;
   FStati.FromInteger(se^.status);
end;

end.
