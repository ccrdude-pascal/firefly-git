{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git branch access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk    5m  Updated to use new Consts unit.
// *****************************************************************************
   )
}

unit LibGit2.Branch;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   {$IFDEF SupportFireflyOVM}
   OVM.Attributes,
   OVM.ListView.Attributes,
   {$ENDIF SupportFireflyOVM}
   LibGit2.Consts,
   LibGit2.Lib;

type
   TGitBranchType = (gbtLocal, gbtRemote, gbtAll, gbtUnknown);

   TGitBranchList = class;

   { TGitBranch }

   TGitBranch = class
   private
      FBranchIsCheckedOut: boolean;
      FBranchIsHead: boolean;
      FBranchName: string;
      FList: TGitBranchList;
      FReference: PGitReference;
      FType: git_branch_t;
      function GetBranchType: TGitBranchType;
   public
      constructor Create(AList: TGitBranchList; AReference: PGitReference; AType: git_branch_t);
      procedure Load;
   published
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Name')]
      {$ENDIF SupportFireflyOVM}
      property BranchName: string read FBranchName;
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Type')]
      {$ENDIF SupportFireflyOVM}
      property BranchType: TGitBranchType read GetBranchType;
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Head'), AOVMBooleanYesNo()]
      {$ENDIF SupportFireflyOVM}
      property BranchIsHead: boolean read FBranchIsHead;
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Checked Out'), AOVMBooleanYesNo()]
      {$ENDIF SupportFireflyOVM}
      property BranchIsCheckedOut: boolean read FBranchIsCheckedOut;
   end;

   { TGitBranchList }

   TGitBranchList = class(TObjectList<TGitBranch>)
   private
      FRepository: PGitRepository;
      FLibrary: TLibGit2Library;
   public
      constructor Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
      function Refresh: boolean;
   end;

implementation

{ TGitBranchList }

constructor TGitBranchList.Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
begin
   FRepository := ARepository;
   FLibrary := ALibrary;
end;

function TGitBranchList.Refresh: boolean;
var
   iResult: integer;
   it: PGitBranchIterator;
   ref: PGitReference;
   typ: git_branch_t;
   branch: TGitBranch;
begin
   Self.Clear;
   Result := FLibrary.git_branch_iterator_new(it, FRepository, GIT_BRANCH_ALL);
   if not Result then begin
      Exit;
   end;
   try
      while FLibrary.git_branch_next(ref, typ, it) do begin
         branch := TGitBranch.Create(Self, ref, typ);
         branch.Load;
         Self.Add(branch);
      end;
   finally
      FLibrary.git_branch_iterator_free(it);
   end;
end;

function TGitBranch.GetBranchType: TGitBranchType;
begin
   case integer(FType) of
      GIT_BRANCH_LOCAL: Result := gbtLocal;
      GIT_BRANCH_REMOTE: Result := gbtRemote;
      GIT_BRANCH_ALL: Result := gbtAll;
      else
      begin
         Result := gbtUnknown;
      end;
   end;
end;

constructor TGitBranch.Create(AList: TGitBranchList; AReference: PGitReference; AType: git_branch_t);
begin
   FList := AList;
   FReference := AReference;
   FType := AType;
end;

procedure TGitBranch.Load;
begin
   FList.FLibrary.git_branch_name(FBranchName, FReference);
   FBranchIsHead := FList.FLibrary.git_branch_is_head(FReference);
   FBranchIsCheckedOut := FList.FLibrary.git_branch_is_checked_out(FReference);
end;

end.
