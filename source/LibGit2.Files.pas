unit LibGit2.Files;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Defaults,
   Generics.Collections,
   {$IFDEF SupportFireflyOVM}
   OVM.Attributes,
   OVM.ListView.Attributes,
   OVM.TreeView.Attributes,
   {$ENDIF SupportFireflyOVM}
   LibGit2.Consts,
   LibGit2.Lib;

type
   TGitFileType = (ftDirectory, ftFile);

   { TGitFile }

   {$IFDEF SupportFireflyOVM}
   [AListViewGrouping(True, 'StatusText')]
   {$ENDIF SupportFireflyOVM}
   TGitFile = class
   private
      FStati: TGitStati;
      function GetStatusText: string;
   protected
      FRelativeFilePath: string;
      FFilename: string;
      FFilePath: string;
      FFileType: TGitFileType;
      FParent: TGitFile;
   public
      property FilePath: string read FFilePath;
      property FileType: TGitFileType read FFileType;
      property Stati: TGitStati read FStati;
   published
      {$IFDEF SupportFireflyOVM}
      [ATreeViewDisplayText(), AListViewSkipField()]
      {$ENDIF SupportFireflyOVM}
      property Filename: string read FFilename;
      {$IFDEF SupportFireflyOVM}
      [ATreeViewParentObjectField()]
      {$ENDIF SupportFireflyOVM}
      property Parent: TGitFile read FParent;
      {$IFDEF SupportFireflyOVM}
      [ATreeViewSkipField()]
      {$ENDIF SupportFireflyOVM}
      property RelativeFilePath: string read FRelativeFilePath;
      {$IFDEF SupportFireflyOVM}
      [AListViewSkipField()]
      {$ENDIF SupportFireflyOVM}
      property StatusText: string read GetStatusText;
   end;

   { TGitFileList }

   TGitFileList = class(TObjectList<TGitFile>)
   private
      FRepository: PGitRepository;
      FLibrary: TLibGit2Library;
      FShowIgnored: boolean;
   public
      constructor Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
      function Refresh: boolean;
      procedure SortByPath;
      property ShowIgnored: boolean read FShowIgnored write FShowIgnored;
   end;

implementation

type

   { TGitFilePathComparer }

   TGitFilePathComparer = class(TInterfacedObject, IComparer<TGitFile>)
      function Compare(const Index1, Index2: TGitFile): integer;
   end;

   { TGitFilePathComparer }

function TGitFilePathComparer.Compare(const Index1, Index2: TGitFile): integer;
begin
   Result := CompareText(Index1.RelativeFilePath, Index2.RelativeFilePath);
end;

{ TGitFile }

function TGitFile.GetStatusText: string;
begin
   Result := FStati.ToString;
end;

{ TGitFileList }

constructor TGitFileList.Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
begin
   FRepository := ARepository;
   FLibrary := ALibrary;
   FShowIgnored := False;
end;

function TGitFileList.Refresh: boolean;
var
   sWorkDir: string;

   function ParseGitDirectory(const ADirectory: string; const AParent: TGitFile): boolean;
   var
      sr: TSearchRec;
      sPath: string;
      sPathRelative: string;
      sName: string;
      f: TGitFile;
      s: TGitStati;
      bShow: boolean;
   begin
      sPath := IncludeTrailingPAthDelimiter(ADirectory);
      if (0 = FindFirst(sPath + '*', faAnyFile - faDirectory, sr)) then begin
         try
            repeat
               if (sr.Name = '.') or (sr.Name = '..') or (sr.Name = '') then begin
                  Continue;
               end;
               sName := sPath + sr.Name;
               sPathRelative := ExtractRelativePath(sWorkDir, sName);
               sPathRelative := StringReplace(sPathRelative, '\', '/', [rfReplaceAll]);
            try
               FLibrary.git_status_file(s, FRepository, sPathRelative);
            except
               s := [];
            end;
               bShow := True;
               if (not Self.ShowIgnored) and (gsIgnored in s) then begin
                  bShow := False;
               end;
               if bShow then begin
                  f := TGitFile.Create;
                  f.FFilename := sr.Name;
                  f.FFilePath := sName;
                  f.FRelativeFilePath := sPathRelative;
                  f.FFileType := ftFile;
                  f.FParent := AParent;
                  f.FStati := s;
                  Self.Add(f);
               end;
            until (0 <> FindNext(sr));
         finally
            FindClose(sr);
         end;
      end;
      if (0 = FindFirst(sPath + '*', faAnyFile, sr)) then begin
         try
            repeat
               if ((sr.Attr and faDirectory) <> 0) and (sr.Name <> '.') and (sr.Name <> '..') and (sr.Name <> '.git')
                  {$ifdef unix}
                  and ((SR.Attr and faSymLink{%H-}) = 0)
               {$endif unix}
               then begin
                  sName := sPath + sr.Name;
                  sPathRelative := ExtractRelativePath(sWorkDir, sName);
                  sPathRelative := StringReplace(sPathRelative, '\', '/', [rfReplaceAll]);
                  try
                     FLibrary.git_status_file(s, FRepository, sPathRelative);
                  except
                     s := [];
                  end;
                  bShow := True;
                  if (not Self.ShowIgnored) and (gsIgnored in s) then begin
                     //bShow := False;
                  end;
                  if bShow then begin
                     f := TGitFile.Create;
                     f.FFilename := sr.Name;
                     f.FFilePath := sName;
                     f.FRelativeFilePath := sPathRelative;
                     f.FFileType := ftFile;
                     f.FParent := AParent;
                     f.FStati := s;
                     Self.Add(f);
                  end;
                  ParseGitDirectory(sName, f);
               end;
            until (0 <> FindNext(sr));
         finally
            FindClose(sr);
         end;
      end;
   end;

begin
   Result := True;
   Self.Clear;
   if not FLibrary.git_repository_workdir(FRepository, sWorkDir) then begin
      Result := False;
      Exit;
   end;
   Result := ParseGitDirectory(sWorkDir, nil);
   SortByPath;
end;

procedure TGitFileList.SortByPath;
begin
   Self.Sort(TGitFilePathComparer.Create);
end;

end.
