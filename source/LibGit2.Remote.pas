{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git remote access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk    5m  Updated to use new Consts unit.
// *****************************************************************************
   )
}
unit LibGit2.Remote;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   {$IFDEF SupportFireflyOVM}
   OVM.ListView.Attributes,
   {$ENDIF SupportFireflyOVM}
   LibGit2.Lib;

type
   TGitRemoteList = class;

   { TGitRemote }

   TGitRemote = class
   private
      FRemoteName: string;
      FRemotePushURL: string;
      FRemoteURL: string;
   protected
      FRemote: PGitRemote;
      FList: TGitRemoteList;
   public
      constructor Create(AList: TGitRemoteList; ARemoteName: string);
      procedure Load;
   published
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Name')]
      {$ENDIF SupportFireflyOVM}
      property RemoteName: string read FRemoteName;
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('URL')]
      {$ENDIF SupportFireflyOVM}
      property RemoteURL: string read FRemoteURL;
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Push URL')]
      {$ENDIF SupportFireflyOVM}
      property RemotePushURL: string read FRemotePushURL;
   end;

   { TGitRemoteList }

   TGitRemoteList = class(TObjectList<TGitRemote>)
   private
      FRepository: PGitRepository;
      FLibrary: TLibGit2Library;
   public
      constructor Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
      function Refresh: boolean;
   end;

implementation

uses
   LibGit2.Consts;

   { TGitRemoteList }

constructor TGitRemoteList.Create(ARepository: PGitRepository;
  ALibrary: TLibGit2Library);
begin
   FRepository := ARepository;
   FLibrary := ALibrary;
   Refresh;
end;

function TGitRemoteList.Refresh: boolean;
var
   r: TGitRemote;
   sl: TStringList;
   s: string;
begin
   Clear;
   sl := TStringList.Create;
   try
      if FLibrary.git_remote_list(sl, FRepository) then begin
         for s in sl do begin
            r := TGitRemote.Create(Self, s);
            r.Load;
            Self.Add(r);
         end;
      end;
   finally
      sl.Free;
   end;
end;

constructor TGitRemote.Create(AList: TGitRemoteList; ARemoteName: string);
begin
   inherited Create;
   FList := AList;
   FRemoteName := ARemoteName;
end;

procedure TGitRemote.Load;
var
   p: pansichar;
   iResult: integer;
begin
   if not FList.FLibrary.git_remote_lookup(FRemote, FList.FRepository, FRemoteName) then begin
      Exit;
   end;
   if not FList.FLibrary.git_remote_url(FRemote, FRemoteURL) then begin
      FRemoteURL := '';
   end;
   if not FList.FLibrary.git_remote_pushurl(FRemote, FRemotePushURL) then begin
      FRemotePushURL := '';
   end;
end;

end.
