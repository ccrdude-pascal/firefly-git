{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git index access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk    5m  Moved from statically to dynamically linked functions.
// 2024-01-29  pk    5m  Updated to use new Consts and Exceptions units.
// *****************************************************************************
   )
}
unit LibGit2.Index;

{$mode Delphi}{$H+}
{$DEFINE UseFireflyOVM}

interface

uses
   Classes,
   SysUtils,
   Generics.Defaults,
   Generics.Collections,
   {$IFDEF SupportFireflyOVM}
   OVM.Attributes,
   OVM.ListView.Attributes,
   {$ENDIF SupportFireflyOVM}
   LibGit2.Lib,
   LibGit2.Consts;

type
   (*
   GIT_INDEX_ENTRY_NAMEMASK    = ($0FFF);
   GIT_INDEX_ENTRY_STAGEMASK   = ($3000);
   GIT_INDEX_ENTRY_STAGESHIFT  = 12;
   GIT_INDEX_ENTRY_EXTENDED    = ($4000);
   GIT_INDEX_ENTRY_VALID       = ($8000);
   // ex
   GIT_INDEX_ENTRY_INTENT_TO_ADD = (1 shl 13);
   GIT_INDEX_ENTRY_SKIP_WORKTREE = (1 shl 14);
   GIT_INDEX_ENTRY_EXTENDED_FLAGS = (GIT_INDEX_ENTRY_INTENT_TO_ADD or GIT_INDEX_ENTRY_SKIP_WORKTREE);
   GIT_INDEX_ENTRY_UPTODATE    = (1 shl 2);

   23 = 00010111
   24 = 00011000
   44 = 00101100
   *)

   TGitIndexList = class;

   { TGitIndex }

   // git_index_has_conflicts
   // git_index_entry_is_conflict

   TGitIndex = class
   private
      FList: TGitIndexList;
      FIndexIndex: integer;
      FEntry: TGitIndexEntry;
      FEntryAvail: boolean;
      function GetCreated: TDateTime;
      function GetFileSizeText: string;
      function GetFlagsRaw: uint16_t;
      function GetFlagsExRaw: uint16_t;
      function GetModified: TDateTime;
      function GetPath: string;
   public
      constructor Create(AList: TGitIndexList; AnIndexIndex: integer);
      procedure Load;
      property IndexIndex: integer read FIndexIndex;
   published
      property Path: string read GetPath;
      {$IFDEF SupportFireflyOVM}
      [AListViewColumnDetails('Size', taRightJustify)]
      {$ENDIF SupportFireflyOVM}
      property FileSizeText: string read GetFileSizeText;
      property Created: TDateTime read GetCreated;
      property Modified: TDateTime read GetModified;
      property FlagsRaw: uint16_t read GetFlagsRaw;
      property FlagsExRaw: uint16_t read GetFlagsExRaw;
   end;

   { TGitIndexList }

   TGitIndexList = class(TObjectList<TGitIndex>)
   private
      FIndex: PGitIndex;
      FRepository: PGitRepository;
      FLibrary: TLibGit2Library;
   public
      constructor Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
      function Refresh: boolean;
      procedure SortByPath;
   end;

implementation

uses
   DateUtils,
   LibGit2.Exceptions;

type

   { TGitIndexPathComparer }

   TGitIndexPathComparer = class(TInterfacedObject, IComparer<TGitIndex>)
      function Compare(const Index1, Index2: TGitIndex): integer;
   end;

{ TGitIndexPathComparer }

function TGitIndexPathComparer.Compare(const Index1, Index2: TGitIndex): integer;
begin
   Result := CompareText(Index1.Path, Index2.Path);
end;

   { TGitIndexList }

constructor TGitIndexList.Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
begin
   FRepository := ARepository;
   FLibrary := ALibrary;
end;

function TGitIndexList.Refresh: boolean;
var
   iCount: integer;
   idx: TGitIndex;
   ifile: integer;
begin
   Self.Clear;
   FLibrary.git_repository_index(FIndex, FRepository);
   iCount := FLibrary.git_index_entrycount(FIndex);
   for iFile := 0 to Pred(iCount) do begin
      idx := TGitIndex.Create(Self, iFile);
      idx.Load;
      if idx.FEntryAvail then begin
         Self.Add(idx);
      end else begin
         idx.Free;
      end;
   end;
   FLibrary.git_index_free(FIndex);
   Self.SortByPath;
end;

procedure TGitIndexList.SortByPath;
begin
   Self.Sort(TGitIndexPathComparer.Create);
end;

function TGitIndex.GetPath: string;
begin
   if not FEntryAvail then begin
      raise TLibGitException.Create('index pointer not available');
   end;
   Result := FEntry.path;
end;

function TGitIndex.GetFileSizeText: string;
begin
   Result := Format('%.0n', [FEntry.file_size + 0.0]);
end;

function TGitIndex.GetFlagsRaw: uint16_t;
begin
   Result := FEntry.flags;
end;

function TGitIndex.GetFlagsExRaw: uint16_t;
begin
   Result := FEntry.flags_extended;
end;

function TGitIndex.GetModified: TDateTime;
begin
   Result := UnixToDateTime(FEntry.mtime.seconds);
end;

function TGitIndex.GetCreated: TDateTime;
begin
   Result := UnixToDateTime(FEntry.ctime.seconds);
end;

constructor TGitIndex.Create(AList: TGitIndexList; AnIndexIndex: integer);
begin
   FList := AList;
   FIndexIndex := AnIndexIndex;
end;

procedure TGitIndex.Load;
var
   p: PGitIndexEntry;
begin
   p := FList.FLibrary.git_index_get_byindex(FList.FIndex, FIndexIndex);
   FEntryAvail := Assigned(p);
   if FEntryAvail then begin
      FEntry := p^;
   end;
end;

end.
