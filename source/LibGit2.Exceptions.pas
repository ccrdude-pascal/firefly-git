{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Custom exceptions for anything git related.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk    5m  Added library handling exceptions.
// 2024-01-26  pk    5m  Added base and bad call exception types.
// *****************************************************************************
   )
}

unit LibGit2.Exceptions;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils;

type
   TLibGitException = class(Exception);

   { TLibGitCallException }

   TLibGitCallException = class(TLibGitException)
   private
      FCall: string;
      FErrorCode: integer;
      FFunctionName: string;
   public
      constructor Create(const AnErrorCode: integer; const AnErrorMessage, AFunction, ACall: string);
      property ErrorCode: integer read FErrorCode;
      property FunctionName: string read FFunctionName;
      property Call: string read FCall;
   end;

   { TLibGitLibraryException }

   TLibGitLibraryException = class(TLibGitException)
   private
      FLibraryName: string;
   public
      constructor Create(const ALibraryName, AMessage: string);
      property LibraryName: string read FLibraryName;
   end;

   { TLibGitLibraryMissingImportException }

   TLibGitLibraryMissingImportException = class(TLibGitLibraryException)
   private
      FFunctionName: string;
   public
      constructor Create(const ALibraryName, AFunction: string);
      property FunctionName: string read FFunctionName;
   end;

implementation

{ TLibGitCallException }

constructor TLibGitCallException.Create(const AnErrorCode: integer; const AnErrorMessage, AFunction, ACall: string);
begin
   inherited Create(AnErrorMessage);
   FErrorCode := AnErrorCode;
   FFunctionName := AFunction;
   FCall := ACall;
end;

{ TLibGitLibraryException }

constructor TLibGitLibraryException.Create(const ALibraryName, AMessage: string);
begin
   FLibraryName := ALibraryName;
   inherited Create(AMessage);
end;

{ TLibGitLibraryMissingImportException }

constructor TLibGitLibraryMissingImportException.Create(const ALibraryName, AFunction: string);
begin
   FFunctionName := AFunction;
   inherited Create(ALibraryName, Format('Function %s was not imported from library %s.', [AFunction, ALibraryName]));
end;


end.
