{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git related consts.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk    5m  Moved from LibGit2.Base since this is a lower level.
// *****************************************************************************
   )
}

unit LibGit2.Consts;

{$mode Delphi}{$H+}
{$modeswitch TypeHelpers ON}

interface

uses
   Classes,
   SysUtils;

type
   int8_t = shortint;
   int16_t = smallint;
   int32_t = integer;
   uint8_t = byte;
   uint16_t = word;
   uint32_t = cardinal;
   int64_t = int64;
   uint64_t = uint64;

const
   GIT_SUCCESS       = 0;
   GIT_ITERATOR_OVER = -31;

   {$IFDEF GIT_EXPERIMENTAL_SHA256}
   GIT_OID_SHA1   = 1; (**< SHA1 *)
   GIT_OID_SHA256 = 2; (**< SHA256 *)
   {$ELSE}
   GIT_OID_SHA1   = 1; (**< SHA1 *)
   {$ENDIF}

   GIT_OID_DEFAULT      = GIT_OID_SHA1;
   GIT_OID_SHA1_SIZE    = 20;
   GIT_OID_SHA1_HEXSIZE = (GIT_OID_SHA1_SIZE * 2);

   {$IFNDEF GIT_EXPERIMENTAL_SHA256}
   GIT_OID_MAX_SIZE    = GIT_OID_SHA1_SIZE;
   GIT_OID_MAX_HEXSIZE = GIT_OID_SHA1_HEXSIZE;
   {$ELSE}
   GIT_OID_MAX_SIZE    = GIT_OID_SHA256_SIZE;
   GIT_OID_MAX_HEXSIZE = GIT_OID_SHA256_HEXSIZE;
   {$ENDIF}

const
   GIT_BRANCH_LOCAL  = 1;
   GIT_BRANCH_REMOTE = 2;
   GIT_BRANCH_ALL    = GIT_BRANCH_LOCAL or GIT_BRANCH_REMOTE;

const
   GIT_STATUS_CURRENT          = 0;
   GIT_STATUS_INDEX_NEW        = (1 shl 0);
   GIT_STATUS_INDEX_MODIFIED   = (1 shl 1);
   GIT_STATUS_INDEX_DELETED    = (1 shl 2);
   GIT_STATUS_INDEX_RENAMED    = (1 shl 3);
   GIT_STATUS_INDEX_TYPECHANGE = (1 shl 4);
   GIT_STATUS_WT_NEW           = (1 shl 7);
   GIT_STATUS_WT_MODIFIED      = (1 shl 8);
   GIT_STATUS_WT_DELETED       = (1 shl 9);
   GIT_STATUS_WT_TYPECHANGE    = (1 shl 10);
   GIT_STATUS_WT_RENAMED       = (1 shl 11);
   GIT_STATUS_WT_UNREADABLE    = (1 shl 12);
   GIT_STATUS_IGNORED          = (1 shl 14);
   GIT_STATUS_CONFLICTED       = (1 shl 15);

type
   git_status_t = integer;
   git_object_size_t = uint64_t;
   git_delta_t = integer;
   git_branch_t = integer;

   TGitStatus = (gsCurrent,
      gsIndexNew, gsIndexModified, gsIndexDeleted, gsIndexRename, gsIndexTypeChange,
      gtWorkDirNew, gsWorkDirModified, gsWorkDirDeleted, gsWorkDirTypeChange, gsWorkDirRenamed, gsWorkDirUnreadable,
      gsIgnored, gsConflicted);

   TGitStati = set of TGitStatus;

   { TGitStatiHelper }

   TGitStatiHelper = type helper for TGitStati
      procedure FromInteger(AValue: git_status_t);
      function ToString: string;
   end;

const
   GitStatusValues: array[TGitStatus] of git_status_t = ( // .
      GIT_STATUS_CURRENT, GIT_STATUS_INDEX_NEW, GIT_STATUS_INDEX_MODIFIED, // .
      GIT_STATUS_INDEX_DELETED, GIT_STATUS_INDEX_RENAMED, GIT_STATUS_INDEX_TYPECHANGE, // .
      GIT_STATUS_WT_NEW, GIT_STATUS_WT_MODIFIED, GIT_STATUS_WT_DELETED, // .
      GIT_STATUS_WT_TYPECHANGE, GIT_STATUS_WT_RENAMED, GIT_STATUS_WT_UNREADABLE, // .
      GIT_STATUS_IGNORED, GIT_STATUS_CONFLICTED
      );

type
   TGitStringArray = record
      strings: PPAnsiChar;
      Count: size_t;
   end;
   PGitStringArray = ^TGitStringArray;

   git_time_t = int64_t;

   TGitTime = record
      time: git_time_t; (**< time in seconds from epoch  *)
      offset: integer;  (**< timezone offset, in minutes  *)
      sign: ansichar;   (**< indicator for questionable '-0000' offsets in signature  *)
   end;
   PGitTime = ^TGitTime;

const
   GIT_SORT_NONE        = 0;
   GIT_SORT_TOPOLOGICAL = 1 shl 0;
   GIT_SORT_TIME        = 1 shl 1;
   GIT_SORT_REVERSE     = 1 shl 2;

type
   git_sort_t = integer;
   TGitSortMode = (gsmNone = 0, gsmTopological = 1, gsmTime = 2, gsmReverse = 4);

implementation


{ TGitStatusHelper }

procedure TGitStatiHelper.FromInteger(AValue: git_status_t);
var
   s: TGitStatus;
begin
   if (GIT_STATUS_CURRENT = AValue) then begin
      Self := [gsCurrent];
      Exit;
   end;
   Self := [];
   for s in TGitStatus do begin
      if (s <> gsCurrent) and ((GitStatusValues[s] and AValue) > 0) then begin
         Self += [s];
      end;
   end;
end;

function TGitStatiHelper.ToString: string;
var
   s: TGitStatus;
   st: string;
begin
   Result := '';
   for s in TGitStatus do begin
      if s in Self then begin
         if Length(Result) > 0 then begin
            Result += ', ';
         end;
         Str(s, st);
         Result += st;
      end;
   end;
end;

end.
