{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git commit list access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-29  pk    5m  Updated to use new Consts unit.
// *****************************************************************************
   )
}

unit LibGit2.Commits;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   {$IFDEF SupportFireflyOVM}
   OVM.Attributes,
   OVM.ListView.Attributes,
   {$ENDIF SupportFireflyOVM}
   LibGit2.Lib;

type
   TGitCommitList = class;

   { TGitCommit }

   TGitCommit = class
   private
      FAuthorEmail: string;
      FAuthorName: string;
      FList: TGitCommitList;
      FMessage: string;
      FOID: TGitObjectID;
      FSignTime: TDateTime;
      function GetOID: string;
      function GetTimestamp: string;
   public
      constructor Create(AList: TGitCommitList; AnOID: TGitObjectID; ACommit: PGitCommit);
   published
      property Timestamp: string read GetTimestamp;
      property Message: string read FMessage;
      property AuthorName: string read FAuthorName;
      property AuthorEmail: string read FAuthorEmail;
      property OID: string read GetOID;
   end;

   { TGitCommitList }

   TGitCommitList = class(TObjectList<TGitCommit>)
   private
      FRepository: PGitRepository;
      FLibrary: TLibGit2Library;
   public
      constructor Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
      function Refresh: boolean;
   end;

implementation

uses
   DateUtils,
   LibGit2.Consts;

   { TGitCommitList }

constructor TGitCommitList.Create(ARepository: PGitRepository; ALibrary: TLibGit2Library);
begin
   FRepository := ARepository;
   FLibrary := ALibrary;
end;

function TGitCommitList.Refresh: boolean;
var
   iResult: integer;
   walker: PGitRevWalk;
   oid: TGitObjectID;
   commit: PGitCommit;
   c: TGitCommit;
begin
   Initialize(oid);
   Result := FLibrary.git_revwalk_new(walker, FRepository);
   if not Result then begin
      Exit;
   end;
   Result := FLibrary.git_revwalk_sorting(walker, gsmTopological);
   if not Result then begin
      Exit;
   end;
   Result := FLibrary.git_revwalk_push_head(walker);
   if not Result then begin
      Exit;
   end;

   try
      while FLibrary.git_revwalk_next(oid, walker) do begin
         if FLibrary.git_commit_lookup(commit, FRepository, oid) then begin
            try
               c := TGitCommit.Create(Self, oid, commit);
               Self.Add(c);
            finally
               FLibrary.git_commit_free(commit);
            end;
         end;
      end;
   finally
      FLibrary.git_revwalk_free(walker);
   end;
end;

{ TGitCommit }

function TGitCommit.GetOID: string;
var
   i: integer;
begin
   Result := '';
   for i := 0 to Pred(GIT_OID_MAX_SIZE) do begin
      Result += IntToHex(FOID.id[i], 2);
   end;
   Result := LowerCase(Result);
end;

function TGitCommit.GetTimestamp: string;
begin
   if (0 = FSignTime) then begin
      Result := 'n/a';
   end else begin
      Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', FSignTime);
   end;
end;

constructor TGitCommit.Create(AList: TGitCommitList; AnOID: TGitObjectID; ACommit: PGitCommit);
var
   i: integer;
   p: pansichar;
   sig: TGitSignature;
begin
   FList := AList;
   FOID := AnOID;
   if not FList.FLibrary.git_commit_message(ACommit, FMessage) then begin
      FMessage := '';
   end;
   FSignTime := 0;
   if FList.FLibrary.git_commit_committer(ACommit, sig) then begin
      p := sig.SigName;
      if Assigned(p) then begin
         FAuthorName := string(ansistring(p));
      end;
      p := sig.SigEmail;
      if Assigned(p) then begin
         FAuthorEmail := string(ansistring(p));
      end;
      FSignTime := UnixToDateTime(sig.SigTime.time);
   end;
end;

end.
