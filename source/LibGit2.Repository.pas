{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Git repository access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-31  pk    5m  Added header.
// *****************************************************************************
   )
}
unit LibGit2.Repository;

{$mode Delphi}{$H+}
{.$DEFINE UseDelphiLibGit}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   {$IFDEF UseDelphiLibGit}
   libgit2,
   {$ENDIF UseDelphiLibGit}
   LibGit2.Lib,
   LibGit2.Remote,
   LibGit2.Branch,
   LibGit2.Status,
   LibGit2.Index,
   LibGit2.Files,
   LibGit2.Commits;

type

   { TGitRepository }

   TGitRepository = class
   private
      function GetBranches: TGitBranchList;
      function GetFilesList: TGitFileList;
      function GetIndex: TGitIndexList;
      function GetLastErrorCall: string;
      function GetRemotes: TGitRemoteList;
      function GetCommits: TGitCommitList;
      function GetRepositoryPath: string;
      function GetRepositoryWorkDir: string;
      function GetStatusList: TGitStatusList;
   protected
      FRepository: PGitRepository;
      FRemotes: TGitRemoteList;
      FIndex: TGitIndexList;
      FBranches: TGitBranchList;
      FCommits: TGitCommitList;
      FStatusList: TGitStatusList;
      FFiles: TGitFileList;
      FLibrary: TLibGit2Library;
   public
      constructor Create;
      destructor Destroy; override;
      function Lib: TLibGit2Library;
      function OpenUsingPath(APath: string; AFindInUpperFolders: boolean = False): boolean;
      function InitializeInPath(APath: string; AIsBare: boolean = False): boolean;
      function AddFile(AFilename: string): boolean;
      function AddFiles(AFilenames: array of string): boolean;
      function Commit(AMessage: string): boolean;
      function GetVersion(out AMajor, AMinor, ARevision: integer): boolean;
      property Remotes: TGitRemoteList read GetRemotes;
      property Index: TGitIndexList read GetIndex;
      property Branches: TGitBranchList read GetBranches;
      property Commits: TGitCommitList read GetCommits;
      property StatusList: TGitStatusList read GetStatusList;
      property Files: TGitFileList read GetFilesList;
      property LastErrorCall: string read GetLastErrorCall;
      property RepositoryHandle: PGitRepository read FRepository;
      property RepositoryPath: string read GetRepositoryPath;
      property RepositoryWorkDir: string read GetRepositoryWorkDir;
   public
      class constructor Create;
      class destructor Destroy;
   end;

function CleanGitDirectory(const ADirectory: string): boolean;

implementation

uses
   LazFileUtils,
   FileUtil,
   LibGit2.Consts,
   LibGit2.Exceptions;

function CleanGitDirectory(const ADirectory: string): boolean;

   function IsDirectoryEmpty(const ADirectory: string): boolean;
   var
      SearchRec: TSearchRec;
      SearchRes: longint;
   begin
      Result := True;
      SearchRes := FindFirst(IncludeTrailingPathDelimiter(ADirectory) + AllFilesMask, faAnyFile, SearchRec);
      try
         while SearchRes = 0 do begin
            if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then begin
               Result := False;
               Break;
            end;
            SearchRes := FindNext(SearchRec);
         end;
      finally
         SysUtils.FindClose(SearchRec);
      end;
   end;

var
   SR: TSearchRec;
   DirName: string;
   Name: string;
begin
   DirName := AppendPathDelim(ADirectory);
   if IsDirectoryEmpty(DirName) then
      RemoveDirUTF8(DirName);
   if FindFirst(DirName + '*', faAnyFile - faDirectory, SR) = 0 then begin
      try
         repeat
            if (SR.Name = '.') or (SR.Name = '..') or (SR.Name = '') then
               Continue;
            Name := DirName + SR.Name;
            if not DeleteFileUTF8(Name) then begin
               FileSetAttrUTF8(Name, faNormal);
               DeleteFileUTF8(Name);
            end;
         until FindNext(SR) <> 0;
      finally
         FindClose(SR);
      end;
   end;
   if FindFirst(DirName + '*', faAnyFile, SR) = 0 then begin
      try
         repeat
            if ((SR.Attr and faDirectory) <> 0) and (SR.Name <> '.') and (SR.Name <> '..')
               {$ifdef unix}
               and ((SR.Attr and faSymLink{%H-}) = 0)
            {$endif unix}
            then begin
               Name := DirName + SR.Name;
               FileSetAttrUTF8(Name, faNormal);
               CleanGitDirectory(DirName + SR.Name);
            end;
         until FindNext(SR) <> 0;
      finally
         FindClose(SR);
      end;
   end;
   Result := DeleteDirectory(ADirectory, False);
end;

{ TGitRepository }

function TGitRepository.GetBranches: TGitBranchList;
begin
   if (not Assigned(FRepository)) then begin
      raise TLibGitException.Create('The branch list is not available without an opened repository.');
   end;
   if not Assigned(FBranches) then begin
      FBranches := TGitBranchList.Create(FRepository, FLibrary);
   end;
   FBranches.Refresh;
   Result := FBranches;
end;

function TGitRepository.GetFilesList: TGitFileList;
begin
   if (not Assigned(FRepository)) then begin
      raise TLibGitException.Create('The files list is not available without an opened repository.');
   end;
   if not Assigned(FFiles) then begin
      FFiles := TGitFileList.Create(FRepository, FLibrary);
   end;
   FFiles.Refresh;
   Result := FFiles;
end;

function TGitRepository.GetIndex: TGitIndexList;
begin
   if (not Assigned(FRepository)) then begin
      raise TLibGitException.Create('The index is not available without an opened repository.');
   end;
   if not Assigned(FIndex) then begin
      FIndex := TGitIndexList.Create(FRepository, FLibrary);
   end;
   FIndex.Refresh;
   Result := FIndex;
end;

function TGitRepository.GetLastErrorCall: string;
begin
   Result := Lib.LastError;
end;

function TGitRepository.GetRemotes: TGitRemoteList;
begin
   if (not Assigned(FRepository)) then begin
      raise TLibGitException.Create('The remote list is not available without an opened repository.');
   end;
   if not Assigned(FRemotes) then begin
      FRemotes := TGitRemoteList.Create(FRepository, FLibrary);
   end;
   FRemotes.Refresh;
   Result := FRemotes;
end;

function TGitRepository.GetCommits: TGitCommitList;
begin
   if (not Assigned(FRepository)) then begin
      raise TLibGitException.Create('The RevWalk list is not available without an opened repository.');
   end;
   if not Assigned(FCommits) then begin
      FCommits := TGitCommitList.Create(FRepository, FLibrary);
   end;
   FCommits.Refresh;
   Result := FCommits;
end;

function TGitRepository.GetRepositoryPath: string;
begin
   if not Lib.git_repository_path(FRepository, Result) then begin
      Result := '';
   end;
end;

function TGitRepository.GetRepositoryWorkDir: string;
begin
   if not Lib.git_repository_workdir(FRepository, Result) then begin
      Result := '';
   end;
end;

function TGitRepository.GetStatusList: TGitStatusList;
begin
   if (not Assigned(FRepository)) then begin
      raise TLibGitException.Create('The status list is not available without an opened repository.');
   end;
   if not Assigned(FStatusList) then begin
      FStatusList := TGitStatusList.Create(FRepository, FLibrary);
   end;
   FStatusList.Refresh;
   Result := FStatusList;
end;

function TGitRepository.Lib: TLibGit2Library;
begin
   if not Assigned(FLibrary) then begin
      FLibrary := TLibGit2Library.Create;
   end;
   Result := FLibrary;
end;

constructor TGitRepository.Create;
begin
   FRepository := nil;
   FRemotes := nil;
   FIndex := nil;
   FBranches := nil;
   FCommits := nil;
   FFiles := nil;
   FLibrary := nil;
end;

destructor TGitRepository.Destroy;
begin
   FLibrary.Free;
   FFiles.Free;
   FRemotes.Free;
   FIndex.Free;
   FBranches.Free;
   FCommits.Free;
   FStatusList.Free;
   inherited Destroy;
end;

function TGitRepository.OpenUsingPath(APath: string; AFindInUpperFolders: boolean): boolean;
var
   sPath: string;
   bFound: boolean;
begin
   if AFindInUpperFolders then begin
      sPath := IncludeTrailingPathDelimiter(APath);
      bFound := FileExists(sPath + '.git\config');
      repeat
         if not bFound then begin
            sPath := ExtractFilePath(ExcludeTrailingPathDelimiter(sPath));
            bFound := FileExists(sPath + '.git\config');
         end;
      until bFound or (Length(sPath) = 3);
      if not bFound then begin
         sPath := APath;
      end;
   end else begin
      sPath := APath;
   end;
   Result := Lib.git_repository_open(FRepository, sPath);
end;

{
  @see https://libgit2.org/docs/examples/init/
}
function TGitRepository.InitializeInPath(APath: string; AIsBare: boolean): boolean;
var
   iResult: integer;
   sig: PGitSignature;
   idx: PGitIndex;
   tree_id: TGitObjectID;
   commit_id: TGitObjectID;
   tree: PGitTree;
begin
   Result := Lib.git_repository_init(FRepository, APath, AIsBare);
   if not Result then begin
      Exit;
   end;
   Result := Lib.git_signature_default(sig, FRepository);
   if not Result then begin
      Exit;
   end;
   try
      Result := Lib.git_repository_index(idx, FRepository);
      if not Result then begin
         Exit;
      end;
      try
         Result := Lib.git_index_write_tree(tree_id, idx);
         if not Result then begin
            Exit;
         end;
         Result := Lib.git_tree_lookup(tree, FRepository, tree_id);
         if not Result then begin
            Exit;
         end;
         //iResult := git_commit_create_v(@commit_id, FRepository, 'HEAD', sig, sig, nil, 'Initial commit.', tree, 0);
         Result := Lib.git_commit_create(commit_id, FRepository, 'HEAD', sig, sig, nil, 'Initial commit.', tree, 0, nil);
         if not Result then begin
            Exit;
         end;
      finally
         Lib.git_index_free(idx);
      end;
   finally
      Lib.git_signature_free(sig);
   end;
end;

function TGitRepository.AddFile(AFilename: string): boolean;
begin
   AddFiles([AFilename]);
end;

function TGitRepository.AddFiles(AFilenames: array of string): boolean;
var
   iResult: integer;
   idx: PGitIndex;
   ps: TGitStringArray;
   a: array of pansichar;
   i: integer;
begin
   Result := Lib.git_repository_index(idx, FRepository);
   if not Result then begin
      Exit;
   end;
   try
      ps.Count := Length(AFilenames);
      SetLength(a, ps.Count);
      for i := 0 to Pred(ps.Count) do begin
         a[i] := pansichar(ansistring(AFilenames[i]));
      end;
      ps.strings := @a[0];
      Result := Lib.git_index_add_all(idx, ps, 0, nil, nil);
      if not Result then begin
         Exit;
      end;
      Result := Lib.git_index_write(idx);
      if not Result then begin
         Exit;
      end;
   finally
      Lib.git_index_free(idx);
   end;
end;

function TGitRepository.Commit(AMessage: string): boolean;
var
   iResult: integer;
   sig: PGitSignature;
   idx: PGitIndex;
   tree_id: TGitObjectID;
   commit_id: TGitObjectID;
   parent_id: TGitObjectID;
   tree: PGitTree;
   parent: PGitCommit;
begin
   Result := Lib.git_signature_default(sig, FRepository);
   if not Result then begin
      Exit;
   end;
   try
      Result := Lib.git_repository_index(idx, FRepository);
      if not Result then begin
         Exit;
      end;
      try
         Result := Lib.git_index_write_tree(tree_id, idx);
         if not Result then begin
            Exit;
         end;
         Result := Lib.git_tree_lookup(tree, FRepository, tree_id);
         if not Result then begin
            Exit;
         end;
         Result := Lib.git_reference_name_to_id(parent_id, FRepository, 'HEAD');
         if not Result then begin
            Exit;
         end;
         Result := Lib.git_commit_lookup(parent, FRepository, parent_id);
         if not Result then begin
            Exit;
         end;
         Result := Lib.git_commit_create(commit_id, FRepository, 'HEAD', sig, sig, nil, pansichar(ansistring(AMessage)), tree, 1, @parent);
         if not Result then begin
            Exit;
         end;
      finally
         Lib.git_index_free(idx);
      end;
   finally
      Lib.git_signature_free(sig);
   end;
end;

function TGitRepository.GetVersion(out AMajor, AMinor, ARevision: integer): boolean;
begin
   Result := Lib.git_libgit2_version(AMajor, AMinor, ARevision);
end;

class constructor TGitRepository.Create;
begin
   {$IFDEF UseDelphiLibGit}
   InitLibgit2;
   {$ENDIF UseDelphiLibGit}
end;

class destructor TGitRepository.Destroy;
begin
   {$IFDEF UseDelphiLibGit}
   ShutdownLibgit2;
   {$ENDIF UseDelphiLibGit}
end;

end.
