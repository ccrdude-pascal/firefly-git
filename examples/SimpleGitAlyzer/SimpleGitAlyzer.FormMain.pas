{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Main form of test browser.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-31  pk    5m  Added header.
// *****************************************************************************
   )
}
unit SimpleGitAlyzer.FormMain;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ComCtrls,
   OVM.ListView,
   OVM.TreeView,
   OVM.VirtualTreeView,
   LibGit2.Lib,
   LibGit2.Consts,
   LibGit2.Repository,
   LibGit2.Files,
   LibGit2.Index,
   LibGit2.Status,
   LibGit2.Remote,
   LibGit2.Commits,
   LibGit2.Branch, VirtualTrees;

type

   { TFormSimpleGitAlyzer }

   TFormSimpleGitAlyzer = class(TForm)
      lvCommits: TListView;
      lvFiles: TListView;
      lvStatusList: TListView;
      lvIndex: TListView;
      lvRemotes: TListView;
      lvBranches: TListView;
      memoDebug: TMemo;
      PageControl1: TPageControl;
      pcFiles: TPageControl;
      tabIndex: TTabSheet;
      tabDebug: TTabSheet;
      tabRemote: TTabSheet;
      tabBranches: TTabSheet;
      tabCommits: TTabSheet;
      tabFiles: TTabSheet;
      tabTortoiseGitStyle: TTabSheet;
      tabVirtualTreeView: TTabSheet;
      tabStatusList: TTabSheet;
      ToolBar1: TToolBar;
      ToolButton1: TToolButton;
      vstFiles: TVirtualStringTree;
      procedure FormShow(Sender: TObject);
      procedure ToolButton1Click(Sender: TObject);
   private
      procedure OpenAndDisplayRepository(APath: string);
   public

   end;

var
   FormSimpleGitAlyzer: TFormSimpleGitAlyzer;

implementation

{$R *.lfm}

uses
   LazFileUtils,
   FileUtil;

{ TFormSimpleGitAlyzer }

procedure TFormSimpleGitAlyzer.FormShow(Sender: TObject);
begin
   OpenAndDisplayRepository(ExtractFilePath(ParamStr(0)));
end;

procedure TFormSimpleGitAlyzer.ToolButton1Click(Sender: TObject);

   procedure PrepareNewRepo(var APath: string);
   var
      sl: TStringList;
   begin
      APath := IncludeTrailingPathDelimiter(APath);
      CleanGitDirectory(APath);
      ForceDirectories(APath);
      sl := TStringList.Create;
      try
         sl.Add('# Firefly Git Test');
         sl.Add('This is just a file added to a local fresh Git repository.');
         sl.SaveToFile(APath + 'README.md');
      finally
         sl.Free;
      end;
   end;

   procedure ChangeRepoFile(var APath: string);
   var
      sl: TStringList;
   begin
      APath := IncludeTrailingPathDelimiter(APath);
      sl := TStringList.Create;
      try
         sl.Add('# Firefly Git Test');
         sl.Add('This is just a file changed after the initial commits.');
         sl.SaveToFile(APath + 'README.md');
      finally
         sl.Free;
      end;
   end;

var
   sPath: string;
   repo: TGitRepository;
begin
   sPath := ExtractFilePath(ParamStr(0)) + 'test';
   PrepareNewRepo(sPath);
   repo := TGitRepository.Create;
   try
      repo.Lib.Calls.Clear;
      repo.InitializeInPath(sPath, False);
      repo.AddFile('README.md');
      repo.Commit('Added README.md');
      ChangeRepoFile(sPath);
      OpenAndDisplayRepository(sPath);
   finally
      memoDebug.Lines.AddStrings(repo.Lib.Calls);
      repo.Free;
   end;
end;

procedure TFormSimpleGitAlyzer.OpenAndDisplayRepository(APath: string);

   procedure More(ARepo: PGitRepository);
   begin
   end;

var
   gitRepo: TGitRepository;
   lRemotes: TGitRemoteList;
   lIndex: TGitIndexList;
   lBranches: TGitBranchList;
   lCommits: TGitCommitList;
   lStatusList: TGitStatusList;
   lFiles: TGitFileList;
var
   iMajor, iMinor, iRevision: integer;
begin
   gitRepo := TGitRepository.Create;
   try
      if gitRepo.GetVersion(iMajor, iMinor, iRevision) then begin
         memoDebug.Lines.Add(Format('gitlib2 version %d.%d.%d', [iMajor, iMinor, iRevision]));
      end else begin
         memoDebug.Lines.Add(gitRepo.LastErrorCall);
      end;

      if not gitRepo.OpenUsingPath(APath, true) then begin
         memoDebug.Lines.Add(gitRepo.LastErrorCall);
         Exit;
      end;

      lRemotes := gitRepo.Remotes;
      lvRemotes.DisplayItems<TGitRemote>(lRemotes);

      lIndex := gitRepo.Index;
      lvIndex.DisplayItems<TGitIndex>(lIndex);

      lBranches := gitRepo.Branches;
      lvBranches.DisplayItems<TGitBranch>(lBranches);

      lCommits := gitRepo.Commits;
      lvCommits.DisplayItems<TGitCommit>(lCommits);

      lStatusList := gitRepo.StatusList;
      lvStatusList.DisplayItems<TGitStatus>(lStatusList);

      lFiles := gitRepo.Files;
      vstFiles.DisplayItems<TGitFile>(lFiles);
      lvFiles.DisplayItems<TGitFile>(lFiles);

      More(gitRepo.RepositoryHandle);
   finally
      gitRepo.Free;
   end;
end;

end.
