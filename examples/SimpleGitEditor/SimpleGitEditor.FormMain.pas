unit SimpleGitEditor.FormMain;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   ExtCtrls,
   SynEdit, SynHighlighterPas,
   LibGit2.Files,
   LibGit2.Repository,
   VirtualTrees,
   OVM.VirtualTreeView;

type

   { TFormSimpleGitEditor }

   TFormSimpleGitEditor = class(TForm)
      Splitter1: TSplitter;
      editMain: TSynEdit;
      SynPasSyn1: TSynPasSyn;
      vstExplorer: TVirtualStringTree;
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      FRepository: TGitRepository;
   public
   end;

var
   FormSimpleGitEditor: TFormSimpleGitEditor;

implementation

{$R *.lfm}

{ TFormSimpleGitEditor }

procedure TFormSimpleGitEditor.FormCreate(Sender: TObject);
begin
   FRepository := TGitRepository.Create;
end;

procedure TFormSimpleGitEditor.FormDestroy(Sender: TObject);
begin
   FRepository.Free;
end;

procedure TFormSimpleGitEditor.FormShow(Sender: TObject);
begin
   FRepository.OpenUsingPath(ExtractFilePath(ParamStr(0)), True);
   vstExplorer.DisplayItems<TGitFile>(FRepository.Files);
end;

end.
