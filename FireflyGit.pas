{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyGit;

{$warn 5023 off : no warning about unused units}
interface

uses
  LibGit2.Commits, LibGit2.Consts, LibGit2.Exceptions, LibGit2.Index, 
  LibGit2.Lib, LibGit2.Remote, LibGit2.Repository, LibGit2.Status;

implementation

end.
